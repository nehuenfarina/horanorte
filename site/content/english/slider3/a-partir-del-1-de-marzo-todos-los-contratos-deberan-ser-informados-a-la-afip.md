+++
author = ""
date = 2021-02-18T03:00:00Z
description = ""
image = "/images/alquileresjpeg.jpeg"
image_webp = "/images/alquileresjpeg.jpeg"
title = "A partir del 1 de marzo todos los contratos deberán ser informados a la AFIP"

+++

**_La resolución publicada en el Boletín Oficial establece sanciones para quienes incumplan y no presenten la información requerida dentro del régimen de registración de contratos de locación de inmuebles._**

La Administración Federal de Ingresos Públicos (AFIP) reglamentó el miércoles el régimen de registración de contratos de locación de inmuebles previsto en la Ley 27.551.

"La herramienta permitirá obtener información relevante para verificar el cumplimiento de las obligaciones fiscales de los sujetos intervinientes en distintas operaciones", destacó la AFIP en un comunicado.

La Resolución General N° 4933, publicada este jueves en el Boletín Oficial, da forma a lo dispuesto por el artículo 16 de la ley N° 27.551.

Aquellos contribuyentes que asuman el carácter de locadores o arrendadores en los contratos quedan obligados a la registración de la operación.

Si bien no son sujetos obligados, la normativa contempla la posibilidad de que los intermediarios (corredores, inmobiliarias y escribanos) registren un contrato en representación de sus clientes.

El régimen también prevé que los sujetos que asuman el carácter de locatarios o arrendatarios podrán informar en forma voluntaria un contrato.

El Régimen de Registración de contratos de Locación de Inmuebles entra en vigencia a partir del 1° de marzo. Los contratos celebrados a partir del 1° de julio de 2020 y que continúen vigentes, así como aquellos concretados durante marzo de 2021, gozarán de un plazo excepcional para su registración hasta el 15 de abril de 2021, inclusive.

La normativa de la AFIP dispone que los contratos alcanzados por el régimen son las locaciones de bienes inmuebles urbanos, arrendamientos sobre bienes inmuebles rurales, locaciones temporarias de inmuebles –urbanos o rurales- con fines turísticos, de descanso o similares.

También están comprendidas las locaciones de espacios o superficies fijas o móviles -exclusivas o no- delimitados dentro de bienes inmuebles como locales comerciales y/o "stands" en supermercados, hipermercados, shoppings, centros, paseos o galerías de compras, etc.

Todos los contratos deberán ser informados en forma digital ingresando con Clave Fiscal en afip.gob.ar al servicio "Registro de Locaciones de Inmuebles - reli-contribuyente".

Los sujetos obligados deberán proporcionar datos vinculados a su rol de locador, a la identificación del inmueble y del resto de los intervinientes, y además, deberán adjuntar en un archivo ".pdf" o ".jpg" el contrato celebrado.

El plazo para informar estas operaciones, tanto para cuando se suscriba el contrato de locación o arrendamiento, o para cuando se modifique uno ya existente, será dentro de los 15 días corridos posterior al acto.

Cuando en los contratos celebrados intervengan intermediarios, éstos podrán registrarlos en representación de los locadores o arrendadores.

En el caso de un condominio, la registración de los contratos por parte de cualquiera de los condóminos libera de la obligación al condominio y a los restantes condóminos, siempre que se haya informado a la totalidad de sus integrantes.

La resolución establece sanciones para quienes incumplan y no presenten la información requerida dentro del régimen de registración de contratos de locación de inmuebles.

Asimismo la ley N° 27.551 dispuso que, cuando se inicien acciones judiciales a causa de la ejecución de un contrato de locación, previo a correr traslado de la demanda, el juez debe informar a la AFIP sobre la existencia del contrato.

El sistema para las comunicaciones judiciales se encontrará disponible a partir del 15 de abril de 2021.