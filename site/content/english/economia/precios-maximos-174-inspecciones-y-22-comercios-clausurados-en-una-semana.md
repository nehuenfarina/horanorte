+++
author = ""
date = 2021-02-12T03:00:00Z
description = ""
image = "/images/602682b4bcea6_1004x565.jpg"
image_webp = "/images/602682b4bcea6_1004x565.jpg"
title = "Precios Máximos: 174 inspecciones y 22 comercios clausurados en una semana"

+++

**_El objetivo de estos controles es verificar que el sector privado cumpla con las regulaciones emitidas desde la Secretaría de Comercio Interior, que tienen como fin proteger el bolsillo de las y los consumidores._**

El Secretaría de Comercio Interior clausuró esta semana 22 establecimientos comerciales, tras realizar 174 inspecciones para controlar el cumplimiento de precios y el normal abastecimiento de productos.

"Desde el lunes al jueves de esta semana se realizaron 174 inspecciones, que dieron como resultado 22 clausuras preventivas por incumplimiento de la resolución de Precios Máximos", informó el Ministerio de Desarrollo Productivo a través de un comunicado.

En este sentido, la Secretaría conducida por Paula Español detalló que en lo que "va del año se concretaron más de 1.500 fiscalizaciones en empresas, comercios de proximidad y supermercados para controlar el cumplimiento de precios y el normal abastecimiento de productos que compran las y los argentinos".

Estas fiscalizaciones forman parte del trabajo que lleva adelante el Estado Nacional a través del cuerpo de relevadores de la Secretaría de Comercio Interior, y en coordinación con los equipos de fiscalizadores de las provincias y los municipios.

"El objetivo de estos controles es verificar que el sector privado cumpla con las regulaciones emitidas desde la Secretaría de Comercio Interior, que tienen como fin proteger el bolsillo de las y los consumidores", señala la información,

En la misma dirección se llevan a cabo "los relevamientos que se realizan para supervisar la correcta implementación en comercios y supermercados de los distintos acuerdos de precios, con el espíritu de corregir eventuales incumplimientos y hacer llegar de manera efectiva las políticas de administración de precios y abastecimiento a todos los hogares argentinos".

Esta semana, además de las clausuras, se labraron 79 actas por no respetar los valores establecidos por la resolución de Precios Máximos, y no presentar declaración jurada con la lista de precios correspondiente.

"La Secretaría de Comercio Interior, junto con las provincias y los municipios, lleva coordinados más de 34.000 operativos de fiscalización de precios y abastecimiento de productos esenciales -alimentos, bebidas, artículos de higiene personal y limpieza - en comercios, fábricas y supermercados de todo el país", destaca la información.

En ese contexto, se "labraron más de 4.000 actas por infracciones a los precios máximos y se realizaron más de 600 clausuras preventivas por incumplimientos en los precios".