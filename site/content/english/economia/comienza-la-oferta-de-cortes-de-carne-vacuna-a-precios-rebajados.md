+++
author = ""
date = 2021-02-01T03:00:00Z
description = ""
image = "/images/carnicerias-min-678x381.jpg"
image_webp = "/images/carnicerias-min-678x381.jpg"
title = "Comienza la oferta de cortes de carne vacuna a precios rebajados"

+++

**_Con un volumen de oferta de 6.000 toneladas por mes, los cortes estarán disponibles en las grandes cadenas de supermercados en todo el país. Se trata de ocho cortes con bajas de hasta 30% respecto de octubre._**

Los cortes de carne a precios rebajados que acordó el Gobierno nacional con frigoríficos exportadores se comenzarán a vender desde esta semana en el Mercado Central y 1.600 bocas de expendio en todo el país.

**Se trata de ocho cortes con bajas de hasta 30% respecto de octubre: tira de asado a $ 399 el kilo, vacío $ 499, matambre $ 549, tapa de asado $ 429, cuadrada/bola de lomo $ 489, carnaza $ 359, falda $ 229 y roast beef $ 399, que se suman a los dos cortes incluidos en Precios Cuidados (carne picada $265 y espinazo $ 110 pesos).**

Con un volumen de oferta de 6.000 toneladas por mes, los cortes estarán disponibles en las grandes cadenas de supermercados en todo el país -Carrefour, Jumbo, Disco, Vea, Coto, Walmart, La Anónima, Día, Libertad-, en carnicerías Friar y en el Mercado Central.

En el Mercado Central, los cortes promocionales se venderán todos los días; mientras que en los supermercados la campaña será los primeros tres miércoles y fines de semana de cada mes.

El acuerdo -de carácter anual- tiene vigencia hasta el 31 de marzo, y luego las partes se volverán a reunir para evaluar las condiciones para su continuidad, siempre bajo un esquema de revisión trimestral.

Al respecto, la secretaria de Comercio Interior, Paula Español, destacó que "la idea era retrotraer los precios a noviembre a partir de que hubo una acelerada en los cortes de carne".

La funcionaria indicó que se busca "sostener los precios hasta el 31 de marzo y apuntando a tener un acuerdo anual, revisarlo a fin de marzo, ver cómo funcionó y monitorearlo para poder extenderlo en el tiempo".

Del acuerdo participaron el Consorcio de Exportadores de Carnes Argentina ABC, la Cámara Argentina de la Industria Frigorífica (Cadif), la Unión de la Industria Cárnica Argentina (Unica), la Federación de Industrias Frigoríficas Regionales (Fifra) y la Asociación de Supermercados Unidos (ASU).

Tras el convenio por la carne, ahora el Gobierno trabaja para alcanzar acuerdos similares para frutas, verduras y hortalizas.