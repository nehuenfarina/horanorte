+++
author = ""
date = 2020-09-22T03:00:00Z
description = "Fernanda Vallejos, la autora del proyecto, sostiene que es imperioso resguardar \"la situación habitacional de las personas y familias que alquilan\"."
image = "/images/5efdb96168b60_1004x565-1.jpg"
image_webp = "/images/5efdb96168b60_1004x565.jpg"
title = "Presentan proyecto de ley para extender congelamiento de alquileres hasta marzo"

+++
###### _Según la Federación de Inquilinos, hay un millón de familias en condiciones de ser desalojadas por las deudas acumuladas._

La diputada nacional **Fernanda Vallejos** presentó este lunes en la Cámara de Diputados un proyecto de ley para extender el congelamiento de alquileres y la suspensión de los desalojos hasta marzo del próximo año, una iniciativa que se suma a la presentada por la senadora María de los Ángeles Sacnun en la Cámara alta.  
  
El proyecto de Vallejos postula que la vigencia del DNU 320/20 -que finaliza el 30 de este mes- debe prorrogarse hasta el 31 de marzo de 2021 y entre los argumentos señala el pedido de las entidades de inquilinos en tal sentido "por la imposibilidad de pagar el alquiler".  
  
"Es fundamental y necesario -agregó- extender por lo menos hasta el mes de marzo de 2021 dicha medida, sobre todo teniendo en cuenta los últimos datos y **la necesidad de continuar resguardando la situación habitacional de las personas y familias que alquilan".**  
  
"Cabe ser destacado que la emergencia producida por la pandemia, con sus consecuencias económicas, torna de muy difícil cumplimiento para una importante cantidad de locatarios y locatarias, trabajadores, trabajadoras, comerciantes, profesionales, industriales, pequeños y medianos empresarios para hacer frente a sus obligaciones en los términos estipulados en los contratos", señaló.

**La palabra de los inquilinos**

  
**El presidente de Inquilinos Agrupados, Gervasio Muñoz**, destacó la iniciativa: "Sabemos que el Gobierno nacional está trabajando en la extensión del decreto y en un plan de desendeudamiento".  
  
En un comunicado, consideró necesario que el Gobierno nacional "lo comunique oficialmente **para frenar la presión de las inmobiliarias y los propietarios** sobre los inquilinos. Los niveles de incertidumbre y angustia son enormes".  
  
"No pueden anunciar la extensión un día antes de que venza el decreto actual, si esperan hasta ese momento, ya miles de familias habrán sido echadas de forma violenta o económicamente de sus casas. Es más sencillo solucionar esto ahora, y no cuando ya estén en la calle”, afirmó.  
  
De acuerdo con la Federación de Inquilinos, **hay aproximadamente un millón de familias en condiciones de ser desalojadas** a partir de octubre por las deudas acumuladas y el 47,4% de los inquilinos del país percibe menos ingresos desde que comenzó la pandemia, y de este segmento el 40,6% está endeudado.