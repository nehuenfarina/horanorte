+++
author = ""
date = ""
description = ""
image = "/images/campo.jpg"
image_webp = "/images/campo.jpg"
title = "Proponen desacoplar precios externos de internos para reducir inflación en alimentos"

+++

**_Sergio Chouza (Universidad Nacional de Avellaneda), la diputada del Frente de Todos Fernanda Vallejos, Fabián Amico (Universidad Nacional de Moreno) y Horacio Rovelli (Flacso) fueron consultados por Télam sobre cómo cuidar la economía doméstica._**

Economistas coincidieron este domingo en que la suba de los precios de la carne y el maíz en el mercado internacional se tradujo en un incremento de los valores domésticos de los alimentos, algo que pone en evidencia la necesidad de desacoplar unos de otros para evitar que se genere un traslado inflacionario a la economía doméstica.

El economista de la Universidad Nacional de Avellaneda Sergio Chouza afirmó a Télam que el hecho de "que suban los precios de los productos que exportamos, a priori es algo positivo", pero puntualizó que "la contraindicación de que haya un ciclo alcista bastante pronunciado en los commodities es justamente que estos a su vez son insumos primordiales de la alimentación del país".

Por eso consideró que "es preciso disponer de desacoples de los precios a partir de la política fiscal y tributaria", y subrayó que "la aplicación de retenciones es una de las herramientas por excelencia de desacople primario sobre estos productos".

También indicó que "está la posibilidad de establecer un esquema de cuotificación dentro de las herramientas de administración del comercio", y remarcó que "se pueden disponer de cuáles son los permisos de exportaciones para los sectores primarios, y fijar que se va a poder exportar una vez abastecido el mercado interno".

Chouza puntualizó que "para los sectores productores exportadores todo lo que tenga que ver con alguna intervención del Estado nunca es bien vista", pero sostuvo que el Gobierno "tiene que armonizar los intereses de todos, no solamente de los sectores productores que tienen la posibilidad de aprovechar este ciclo alcista que redunda en mayor rentabilidad, sino también de la población que consume de manera directa o indirecta estos productos de exportación".

A su criterio, "el rol del Estado es fundamental", por lo cual consideró "totalmente lógico y sensato que no se quede de brazos cruzados viendo cómo aumentan los alimentos".

Por su parte, la economista y diputada del Frente de Todos Fernanda Vallejos señaló a esta agencia que "el problema de Argentina es que exportamos lo que comemos, o lo que come el ganado que comemos, por ejemplo el maíz".

"Por eso, cuando aumenta el precio de nuestras exportaciones, algo bienvenido, se tensiona el mercado interno, el precio de los alimentos. Ergo: hay que desacoplar precios", afirmó Vallejos.

Explicó que ese desacople debe hacerse "por dos motivos: justicia social y para acotar la inflación importada, que es el impacto del precio internacional de la canasta de exportaciones" sobre la economía doméstica.

> "Si no, se entra en una carrera precios contra salarios, donde pierden los laburantes, se debilita el mercado interno y no hay crecimiento", precisó la legisladora, quien destacó que "hay muchas herramientas, retenciones, cupos, instrumentos compensadores. Lo esencial es tener muy claro el objetivo de la política, que es lo irrenunciable".

En tanto, el economista de la Universidad Nacional de Moreno Fabián Amico sostuvo a Télam que “la suba del precio internacional (de las materias primas) es equivalente al efecto de una devaluación del tipo de cambio en el precio de los alimentos”.

Advirtió que “cuando mejorás los valores de las exportaciones, que es imprescindible, empeorás los salarios, porque se encarecen los insumos que se utilizan para producir alimentos y eso se traduce en un incremento de los precios internos”.

Señaló que "hubo aumentos muy importantes en el precio internacional del maíz, que en Argentina se utiliza como insumo para producir pollo, cerdo y carne vacuna".

> "Si se analiza el IPC del Indec en forma desagregada se van a encontrar aumentos desproporcionados en los precios del pollo, de la carne picada, y en rubros que tienen como insumo el maíz", destacó Amico.

Remarcó que "claramente eso fue lo que impulsó el IPC al alza (4%) en diciembre, ya que tiene relación bastante directa con el aumento de precios de las exportaciones".

A su criterio, "la alternativa de prohibir las exportaciones no es la mejor porque necesitamos dólares", pero indicó que "surgen dos alternativas que se podrían combinar".

> Precisó que "una es el derecho de exportación, que compensa la suba del precio internacional sin afectar las cantidades", y añadió que "la otra alternativa es que el Gobierno subsidie los aumentos de costos que provoca la utilización de ese insumo, en este caso el maíz a un mayor precio, así los bienes finales no aumentan tanto".

Por su lado, el economista de Flacso Horacio Rovelli afirmó a esta agencia que "la mejor manera de desacoplar los precios externos de los internos son las retenciones", y consideró que "hay que subirlas y segmentarlas por tipo de productor".

En ese sentido señalo que "no es lo mismo que produzca un pequeño chacarero a que lo haga un gran hacendado, o una gran acopiadora", por lo que sostuvo que "el camino lógico para desacoplar los precios internos y externos son las retenciones".

Consideró que "no puede ser que tengamos una retención del maíz y del trigo de solamente el 12%", y opinó que "habría que llevarlas al 30%, lo mismo que con la carne, sobre todo la vacuna".

> Rovelli puntualizó que "todos los países priorizan su demanda interna", y afirmó que "con las retenciones se igualan precios internos y externos, a un valor más beneficioso para el mercado local".

> Así concluyó que "las retenciones son el mecanismo más equitativo, lógico, sencillo, fácil y directo de desacoplar los precios internos y externos", y destacó que "significan mayores ingresos para el Estado y menores precios para el consumo de esos alimentos".