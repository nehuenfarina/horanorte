+++
author = ""
date = 2020-12-21T05:00:00Z
description = ""
image = "/images/alberto-y-cristina-fernandez-951627.jpg"
image_webp = ""
title = "Alberto a Cristina: \"Tengo gratitud con cada ministro, hay que tener coraje para hacerse cargo de la Argentina arrasada\""

+++
**_El presidente defendió a su gabinete luego de que la vice pidiera que "busquen otro laburo" quienes "no se animan"._**

Alberto Fernández expresó su gratitud con "cada ministro, cada gobernador y cada intendente" en una respuesta directa a Cristina Kirchner, que el viernes había pedido que los ministros que "no se animen que busquen otro laburo".

"Ahora, todo este esfuerzo no es el esfuerzo de un Presidente. Es el esfuerzo de todo un gobierno y es el esfuerzo de 24 gobernadores que se pusieron codo a codo a mi lado a pelearle a la pandemia y a pelearle a una economía que se caía a pedazos. Por lo tanto, yo solo tengo gratitud y reconocimiento para con cada ministro mío, cada funcionario mío, por cada empleado que en este Gobierno Nacional que trabajó a mi lado para mantener en pie a la Argentina", indicó el presidente 

"Hay que tener coraje para hacerse cargo de la Argentina arrasada y seguir gobernado cuando una pandemia se lleva puesto al mundo. A ese coraje, gracias", afirmó Alberto  al poner en funcionamiento el proyecto de Capitales Alternas en la ciudad fueguina de Río Grande.

Alberto se mostró junto a casi todo su gabinete en la provincia austral más austral del planeta. Estuvo acompañado por el jefe de Gabinete, Santiago Cafiero; los ministros del Interior, Eduardo de Pedro; Mujeres, Elizabeth Gómez Alcorta; Desarrollo Productivo, Matías Kulfas; Transporte, Mario Meoni; Obras Públicas, Gabriel Katopodis, y Desarrollo Territorial y Hábitat, Jorge Ferraresi. También participaron los secretarios General de la Presidencia, Julio Vitobello, y de Comunicación y Prensa, Juan Pablo Biondi.