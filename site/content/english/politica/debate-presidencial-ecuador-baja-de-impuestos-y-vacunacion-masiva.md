+++
author = ""
date = 2021-01-18T03:00:00Z
description = ""
image = "/images/ecuador.jpg"
image_webp = "/images/ecuador.jpg"
title = "Debate presidencial Ecuador: Baja de impuestos y vacunación masiva"

+++

**_Organizado por el Consejo Nacional Electoral (CNE), y transmitido por radio y televisión, el debate reunió a los 16 aspirantes presidenciables que aspiran a suceder al mandatario Lenin Moreno. Según las encuestas, los dos candidatos con más chances de pasar a un balotaje presidencial son Arauz, el delfín elegido por el expresidente Rafael Correa._**

A tres semanas de las elecciones, los candidatos presidenciales de Ecuador contrastaron sus principales propuestas en un debate televisivo, marcado por los llamados a bajar los impuestos y generar empleo, en medio de una profunda crisis económica, y de comenzar a vacunar masivamente lo antes posible, al mismo tiempo que el país registró su máximo pico de casos diarios de coronavirus.

"Tenemos el cronograma de vacunación listo para iniciar con los profesionales de la salud", prometió anoche en el debate televisivo Andrés Arauz, el candidato del correísmo y de la alianza Unes; mientras su principal rival, el conservador y liberal, Guillermo Lasso, se concentró en su propuesta impositiva para reactivar la economía, en caída libre y atada a un estricto programa del FMI.

"Vamos a bajar el impuesto a la salida de capitales", prometió Lasso, mientras que otros dos candidatos, el conservador Guillermo Celi y el expresidente destituido Lucio Gutiérrez, se enfocaron en pedir una "baja del IVA", según replicó la agencia de noticias AFP.

Organizado por el Consejo Nacional Electoral (CNE), y transmitido por radio y televisión, el debate reunió a los 16 aspirantes presidenciables que aspiran a suceder al mandatario Lenin Moreno, sumido en una fuerte crisis de liderazgo y credibilidad, tras un año de pandemia, que no para de golpear al país, y de una crisis económica, que sigue prometiendo salir con ajustes al gasto público y flexibilización laboral.

Ecuador, un país de 17,4 millones de habitantes, ya suma 230.808 casos de coronavirus y 14.316 muertos, y ayer registró su máximo pico diario de contagios, con cerca de 4.000.

Según las encuestas, los dos candidatos con más chances de pasar a un balotaje presidencial son Arauz, el delfín elegido por el expresidente Rafael Correa, quien fue inhabilitado para presentarse por una reciente condena firme de corrupción, y Lasso, un dirigente con más posibilidades de encontrar aliados de derecha, centro-derecha y centro en una segunda vuelta.