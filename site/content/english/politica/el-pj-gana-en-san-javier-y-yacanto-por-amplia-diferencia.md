+++
author = ""
date = 2021-01-18T03:00:00Z
description = ""
image = "/images/elecciones_yacanto_san_javier_1610932199.jpg"
image_webp = "/images/elecciones_yacanto_san_javier_1610932199.jpg"
title = "El PJ gana en San Javier y Yacanto por amplia diferencia"

+++

**_En las elecciones para suceder al fallecido jefe comunal Roberto Altamirano. el peronismo superó por 48 puntos al opositor Frente Cívico y por 57 a la alianza PRO-UCR._**

Fue consagrado intendente el dirigente Martín García, quien sucederá al fallecido jefe comunal Roberto Altamirano.

El candidato de Hacemos por Córdoba obtuvo el 65% de los votos (930); segundo quedó Rodrigo Murúa Samper, del Frente Cívico de Luis Juez, con el 17% (242) y la alianza Ahora San Javier y Yacanto, del PRO-UCR, consiguió 8% de los votos (108).

El nuevo jefe comunal, de 31 años, asumiría el miércoles para completar el mandato de su antecesor, hasta el año 2023.

La principal referencia política en esa zona es el presidente provisorio de la Unicameral cordobesa, Oscar González, quien felicitó al ganador, igual que el gobernador, Juan Schiaretti.

"Hemos realizado un buen trabajo y la gente lo valoró. Nací en este pueblo, me crié acá y ahora voy a tener la responsabilidad de gobernarlo", dijo García.

El peronismo gobierna en esa localidad desde el retorno de la democracia, en 1983.

Otras agrupaciones, como el Frente Vecinal San Javier y Yacanto y el FIT, lograron el 7 y el 3% respectivamente.

San Javier y Yacanto es una localidad situada en el centro-oeste de la provincia de Córdoba y está constituida por esos dos pueblos contiguos.

"Quiero felicitar a Martín García por la victoria que ha obtenido y el respaldo de su pueblo para ser el nuevo intendente de SanJavier y Yacanto, uno de los lugares más bonitos que tiene nuestra Córdoba", dijo Schiaretti en Twitter, quien extendió el saludo a "todos los habitantes" de ese lugar.

En noviembre pasado el peronista Juan Manuel Llamosas fue reelecto para un segundo período como intendente de Río Cuarto (una de las designadas 24 Capitales Alternas), al obtener el 41% de los votos.