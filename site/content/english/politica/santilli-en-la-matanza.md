+++
author = ""
date = 2020-12-01T15:00:00Z
description = ""
image = "/images/santilli.jpg"
image_webp = ""
title = "Santilli en La Matanza"

+++
**_El vicejefe de Gabinete visitó la ciudad bonaerense junto a Alejandro Finocchiaro y la concejal Pamela Loisi. Y recorrió cooperativas con Héctor "Toty" Flores._**

Diego Santilli visitó la Cooperativa La Juanita en Laferrere junto al ex Ministro de Educación, Alejandro Finocchiaro y la concejal Pamela Loisi. Allí fue recibido por el dirigente de la Coalición Cívica y Diputado Nacional por la Provincia de Bs. As., Toty Flores y su hija Silvia, Directora Ejecutiva de la Cooperativa.

"Hoy vinimos a visitar un grupo de vecinos que se ganan el pan con trabajo y pasión por lo que hacen. Se organizaron, aprendieron un oficio noble y están saliendo adelante por su propia fortaleza", destacó.

Santilli recorrió las instalaciones de la cooperativa, que funciona desde el 2001 y que brinda un fin social, al dar empleo y contratar mano de obra. La organización, trabaja en la generación de emprendimientos productivos y educativos propios, como jardines y talleres. 

También visitó la panadería la "Masa Crítica" y se sumó a una acción social como voluntario junto a Roberto Moritán, asiduo colaborador de la cooperativa, en la elaboración del pan dulce con la receta de Maru Botana. 

Por su parte, la pastelera hizo un vivo en el que contó el paso a paso de la realización del pan dulce, que luego se comercializa en diferentes zonas y locales. Con el producto de esa venta La Juanita sostiene el pan social, más de 200 kilos diarios de un producto que llega al barrio a precio reducido.

Posteriormente, Santilli y Finocchiaro llegaron a la fábrica El Venta, en Rafael Castillo, donde se producen mosaicos y baldosones, entre ellos algunos de los que utilizan en veredas de CABA. Conversaron con Juan Carlos y su hijo Nahuel, dueños de la empresa, quienes explicaron el proceso productivo que ocupa a 60 personas, todas habitantes del Partido.

La recorrida finalizó con un café en la confitería Tokio, ubicada en el centro de San Justo, al que se sumaron el diputado nacional Hernán Berisso y el concejal Gustavo Ferragut.