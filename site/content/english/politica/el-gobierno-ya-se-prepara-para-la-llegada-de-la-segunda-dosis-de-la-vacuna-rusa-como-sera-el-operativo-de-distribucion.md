+++
author = ""
date = 2021-01-15T03:00:00Z
description = ""
image = "/images/2-do-vuelo.jpg"
image_webp = "/images/2-do-vuelo.jpg"
title = "El Gobierno ya se prepara para la llegada de la segunda dosis de la vacuna rusa: cómo será el operativo de distribución"

+++
**_El avión que traerá el segundo componente de la Sputnik V arribará al aeropuerto internacional de Ezeiza mañana al mediodía. Hasta el momento se inocularon un total de 138.218 personas en 477 localidades de todo el país_**

El avión que traerá a la Argentina la segunda dosis de la Sputnik V partió anoche, pasadas las 21, rumbo a Moscú. El retorno al aeropuerto internacional de Ezeiza está previsto para mañana al mediodía, cuando el Gobierno pondrá en marcha la continuidad del plan de inoculación contra el COVID-19. Se trata de 300 mil vacunas que se utilizarán para completar la inmunización de quienes ya se aplicaron la primera dosis. 

Según reveló un reporte difundido por Nomivac (Registro Federal de Vacunación Nominalizado), hasta el momento se inmunizaron 138.218 personas en 477 localidades de todo el país, todas ellas personal de la salud. Buenos Aires es la provincia con más cantidad de vacunados, con un total de 46.670 personas, seguida por Córdoba (14.123), Santa Fe (8.582) y la Ciudad de Buenos Aires (7.950).

Tras 16 horas de ida, la aeronave de Aerolíneas Argentina llegará este viernes en la tarde a Rusia. Luego, se destinarán entre cuatro y cinco horas para la carga de la Sputnik V y los trámites aduaneros. Finalmente, el vuelo de regreso se estima en 18 horas, por lo que tocará suelo argentino el sábado al mediodía aproximadamente. A partir de ese momento, se llevará a cabo un operativo de distribución similar al diseñado para la primera dosis.

Las 300 mil vacunas que llegaron el día de Navidad fueron almacenadas en un predio de la empresa Andreani y recién comenzaron a ser aplicadas el martes 29 de ese mes. De ese lote, la provincia de Buenos Aires recibió 123.000 dosis; Santa Fe, 24.100; la Ciudad Autónoma de Buenos Aires, 23.100; Córdoba, 21.900; Tucumán, 11.500; Mendoza, 11.100; Entre Ríos, 10.100; Salta, 8.300; Chaco, 7.700; Corrientes, 6.700; Santiago del Estero, 5.900; Misiones, 5.200; San Juan, 4.700; Jujuy, 4.600; Río Negro, 4.400; Neuquén, 3.600; Formosa, 3.400; San Luis, 3.300; Chubut, 3.000; Catamarca, 2.800; La Rioja, 2.600; Santa Cruz, 2.400; La Pampa, 2.300 y Tierra del Fuego, 1.300.

Si lo pactado con las autoridades rusas se cumple, antes de fin de enero llegarían 5 millones de dosis de la Sputnik V. “Sobre el final de enero tenemos 4 millones de la primera dosis y 1 millón de la segunda dosis”, especificó el presidente Alberto Fernández en la última entrevista concedida en 2020.

La primera etapa del plan de vacunación terminaría sobre el cierre de febrero. Para ese momento, deberían haber llegado casi 20 millones de dosis para inmunizar a 10 millones de argentinos. Para eso se precisan al menos 20 vuelos de Aerolíneas, aunque la cifra dependerá de que finalmente todas esas dosis estén disponibles.

La llegada del segundo cargamento se da en el contexto de una reciente polémica con la secretaria de Acceso a la Salud, Carla Vizzotti. “La decisión sanitaria más importante que nos tenemos que plantear es si queremos tener 10 millones de personas vacunadas a marzo con dos dosis o si preferimos tener 20 millones de personas con una sola. 

Finalmente, Vizzotti dio marcha atrás y se retractó en redes sociales: “La vacuna Sputnik V es la única cuyo esquema de vacunación consta de dos componentes, en primer lugar, el componente Ad26, y luego de un intervalo mínimo de 21 días, el segundo componente Ad5. Argentina planea administrarlas según esta indicación”.