+++
author = ""
date = 2021-02-09T03:00:00Z
description = ""
image = "/images/1494716497_330000_1494717216_noticia_normal.jpg"
image_webp = "/images/1494716497_330000_1494717216_noticia_normal.jpg"
title = "La ONU reveló que Irán y Corea del Norte volvieron a trabajar juntos en el desarrollo de misiles de largo alcance"

+++

**_“La cooperación habría incluído la transferencia de partes críticas, y los últimos envíos habrían tenido lugar en 2020”, indica un párrafo de un reporte confidencial elaborado por el organismo internacional_**

Irán y Corea del Norte cooperaron en proyectos para desarrollar misiles de largo alcance el año pasado, reveló un informe confidencial de las Naciones Unidas (ONU). El documento podría poner presión sobre la administración de Joe Biden para que responde a una de sus primeras crisis geopolíticas de magnitud.

“Esta renovada cooperación habría incluído la transferencia de partes críticas. El envío más reciente asociado a esta relación tuvo lugar durante el 2020″, indica un párrafo del reporte, elaborado por un panel de expertos que monitorea las sanciones impuestas a Corea del Norte.

Usualmente en los márgenes de la diplomacia internacional, Corea del Norte e Irán han mantenido una larga relación que ha beneficiado a ambos. El panel de la ONU recibió información que mostró que el centro de investigación iraní Shahid Haj Ali Movahed recibió “apoyo y asistencia” de especialistas en misiles norcoreanos, y que Corea del Norte estuvo involucrado en ciertos envíos a Irán.

No quedó inmediatamente claro que había en esos envíos ni cuan significativa fue la cooperación para cada nación. Pero la cooperación renovada entre Irán y Corea del Norte implicaría una derrota en la estrategia de presión de la administración de Donald Trump contra ambos países, y llevaría a que la administración de Biden deba acelerar su estrategia con respecto a ellos, y a quienes ve como países que patrocinan el terrorismo.

El reporte detalla que, en respuesta a las alegaciones, Irán le dijo al panel que “un análisis preliminar de la información provista por el panel indica que podría haberse usado información y datos falsos en sus investigaciones y análisis”.

El secretario de Estado de Estados Unidos, Antony Blinken, ha dicho que la administración de Biden está revisando la estrategia diplomática llevada a cabo por el predecesor de Biden, Donald Trump. También ha dicho que Irán debe volver a cumplir con los requisitos del acuerdo nuclear del 2015 -abandonado por la administración de Trump- antes de que los Estados Unidos consideren levantar sanciones impuestas a Teherán.

#### Kim alardea de un enorme misil móvil

El régimen de Kim Jong-un ha presentado numerosos modelos de nuevos misiles balísticos durante los últimos meses. Son más grandes o más fáciles de mover y disparar que los anteriores. En un desfile militar en Pyongyang que tuvo lugar en octubre, mostró un misil balístico intercontinental (ICBM) enorme que parecía ser el misil pasible de ser transportado por tierra más grande del mundo. También parecía ser capaz de albergar múltiples cabezas de misiles.

El panel hizo referencia a un análisis realizado por un Estado miembros que indicó que es “altamente probable que un dispositivo nuclear pueda ser montado en ese ICBM y también es posible que se puedan montar dispositivos nucleares en los misiles de menor alcance”. “El estado miembro, no obstante, dijo que no queda claro si Corea del Norte desarrolló misiles balísticos que son resistentes al calor que genera durante el momento en el que vuelven a entrar a la atmósfera”, explicó el panel.

El panel también analizó los drones mostrados por Corea del Norte durante el desfile de octubre. Los identificó como “Mavic 2 Pro type” y dijo que fueron manufacturados por la compañía china SZ DJI Technology Co. Ltd. La compañía todavía debe responder a las consultas del panel, concluyó el documento.