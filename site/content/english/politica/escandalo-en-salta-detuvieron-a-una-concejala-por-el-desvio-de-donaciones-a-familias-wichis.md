+++
author = ""
date = 2021-02-09T03:00:00Z
description = ""
image = "/images/1612793920928.jpg"
image_webp = "/images/1612793920928.jpg"
title = "Escándalo en Salta: detuvieron a una concejala por el desvío de donaciones a familias wichís"

+++

**_Un fiscal penal imputó a una concejala radical de Tartagal y a su hermana, que es defensora oficial, por el presunto desvío de donaciones de mercadería destinadas a comunidades originarias del norte salteño por la emergencia socio-sanitaria declarada a principios de 2020._**

Según informó el Ministerio Público de Salta, el fiscal interino penal 1 de Tartagal Rafael Medina imputó a Paola Alejandra Díaz, concejala del radicalismo de 43 años, y a Rosa Fabiola Díaz, de 40, defensora oficial. Fuentes judiciales detallaron que la primera quedó detenida y optó por declarar. Mientras que la segunda se abstuvo de brindar declaraciones, y solo fue imputada y suspendida por su inmunidad a raíz de su cargo en el Juzgado de Garantías 1 de Tartagal.

El escándalo se disparó a raíz de una denuncia de la hermana mayor de ambas, Sandra Díaz, que las acusó en una denuncia periodística de vender donaciones recibidas hace un año para las comunidades wichí de la zona. Lo hizo por video a través del canal Video Tar, de Tartagal, y otros medios.

El fiscal Medina intervino de oficio días pasados una vez conocida la noticia de posibles irregularidades, acopio y venta con la ayuda solidaria. Ese mismo día, el funcionario dispuso las medidas, como una consigna permanente donde se encontraban las donaciones provenientes de distintas fundaciones e invitó a la denunciante a radicar una denuncia formal. Finalmente, la mujer la concretó en la Comisaría 45 de Tartagal y luego amplió en la sede fiscal.

Según la denuncia, en enero de 2020 las acusadas recibieron la donación de mercaderías perecederas, ropa, zapatillas, alcohol en gel, agua mineral y pañales, entre otras cosas, y precisó que sus hermanas habrían mantenido ocultas las donaciones todo el año y hasta ahora para venderlas a negocios de la zona.

La intención, según Sandra Díaz, era mantenerlas guardadas hasta que Paola Díaz se postulara como candidata a diputada provincial “lucrando con dichas mercaderías para un beneficio propio”. “Le expliqué (al fiscal) el nombre de las personas que vendieron las cosas”, sostuvo la hermana denuncia. Y reveló que la metodología consistía en vender bolsones de ropa en las ferias de la zona o en Bolivia.

En uno de los videos, donde hace un descargo público, la concejala manifestó que es parte de un grupo que hace beneficencia y que las cosas que se mostraron en el allanamiento “son cosas que teníamos que llevar a Santa Victoria Este, pero pasó la pandemia”. Entre llantos, aseguró que el donativo se hizo con la Fundación Noble de Santa Fe, cuyos representantes “saben que las cosas están acá documentadas” y “en ningún momento se vendió nada”. Admitió que la mercadería estaba destinada para las comunidades wichí de la zona y que lo que quedaba en la vivienda estaba pendiente de entrega.

La imputación señala que las acusadas tenían a su cargo “la administración y cuidados de los bienes provenientes de las fundaciones, llevando a cabo una administración fraudulenta de los bienes asignados, perjudicando intereses ajenos con su accionar infiel”, pues la mercadería debía ser repartida “entre personas de escasos recursos y de las misiones aborígenes de la etnia wichi”.

En el allanamiento, con auxilio de la fuerza pública, se secuestraron bidones y botellas de agua, ropa nueva y usada y zapatillas. Fuentes de la Defensoría General de la Provincia de Salta informaron que se dispuso un sumario administrativo contra la defensora investigada, quien fue suspendida por 30 días.

Ante el escándalo, la Unión Autónoma de Comunidades Originarias del Pilcomayo (UACOP), junto a las comunidades originarias de Santa Victoria Este del departamento salteño de Rivadavia, anunciaron protestas contra las funcionarias. Durante el fin de semana, los caciques Cándido Mansilla, de la organización La Nueva Generación, y Celina Juárez, de la comunidad Lapacho II, fueron a la casa de la concejala para exigir la entrega de los donativos.

Desde ayer, tras la detención y los allanamientos, las comunidades del Pilcomayo se encuentran acampando en el complejo deportivo municipal, a la espera de la entrega de las donaciones que se encuentran acopiadas en las dependencias de la Policía Provincial.

Las mujeres están imputadas por la supuesta comisión del delito de “administración fraudulenta de bienes o intereses ajenos, en grado de coautoras”.