+++
author = ""
date = 2021-02-17T03:00:00Z
description = ""
image = "/images/gines.jpg"
image_webp = "/images/gines.jpg"
title = "Ginés González García: \"Estamos empezando un ritmo vacunatorio más acelerado\""

+++

**_El ministro destacó que "ya se está finalizando con la vacunación del personal sanitario y comenzando con los adultos mayores, los más vulnerables, con un gradualismo entre los 80 y 70 años según la organización de cada provincia"._**

El ministro de Salud, Gines González García, afirmó que se está comenzando "con un ritmo de vacunación más acelerado" con la llegada esta madrugada de una partida de 580 mil vacunas contra el coronavirus producidas en India, y anunció que se encuentra "en los detalles finales" el acuerdo con China para la provisión de dosis de Sinopharm.

"Hoy es un gran día. En 48 horas, las vacunas llegadas esta madrugada estarán ya disponibles en las provincias", anunció González García en declaraciones a la prensa, tras supervisar el cargamento en el Hangar de Cargas Argentinas del Aeropuerto Internacional de Ezeiza.

El vuelo QR 8155, procedente de Doha, aterrizó en Ezeiza a las 2.58 con el cargamento de vacunas de la marca Covishield, producidas por el laboratorio Serum Institute de la India y desarrolladas por la Universidad de Oxford y AstraZeneca.

"Estamos empezando otro ritmo vacunatorio más acelerado, en negociaciones con los distintos proveedores de vacunas y vamos a seguir acelerando día a día", destacó el funcionario.

En ese marco, el funcionario indicó que ya se está "finalizando con la vacunación del personal sanitario, quedan muy pocos en todo el país, y comenzando con los adultos mayores, los más vulnerables, con un gradualismo entre los 80 y 70 años según la organización de cada provincia".

Previamente, en declaraciones a radio La Red, indicó que "estamos en los detalles finales" del acuerdo con China para la provisión de la producida por Sinopharm.

"Faltaba alguna documentación del Anmat, que ya la están enviando. Será cuestión de días", siguió el ministro, y detalló que "la parte política-comercial está cerrada, nos falta la parte de seguridad que tenemos que tener en cualquier vacuna".