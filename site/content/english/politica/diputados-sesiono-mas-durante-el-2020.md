+++
author = ""
date = ""
description = ""
image = "/images/massa-con-diputados.jpg"
image_webp = ""
title = "Diputados sesionó más durante el 2020"

+++
**_Este año se sancionaron el doble de leyes y se incrementaron las reuniones de comisión, a través del sistema remoto que funcionó por la pandemia._**

La Cámara de Diputados sancionó 66 leyes este año, tuvo 323 reuniones de trabajo en sus distintas comisiones y recibió a 182 funcionarios públicos, a través de un funcionamiento remoto implementado por la pandemia del Covid 19.

El sistema permitió intensificar la activdad, poruqe en 2019 sólo se sancionaron 39 leyes. El trabajo remoto fue implementado por la presidencia de la Cámara e incluyó un sistema de videoconferencias, la inserción de firmas digitales y la trazabilidad del voto de cada diputado.

"No solo permitió resguardar la salud de los legisladores y los trabajadores parlamentarios, sino que implicó un salto cualitativo en el proceso de modernización de la Cámara", destacaron desde despacho presidencial, a través de un comunicado.

El 13 de mayo del corriente año, se llevó adelante la primera sesión mixta (presencial/remota) de la historia de la HCDN. Con este fin, se desarrolló una aplicación propia de participación remota con certificación de identidad y se adaptó el recinto y la plataforma de videoconferencias para realizar sesiones virtuales mixtas.

En total, desde el 10 de diciembre de 2019, se realizaron 22 sesiones, de las cuales 15 fueron realizadas de forma remota, y dos Asambleas Legislativas, para la asunción y apertura de las sesiones del presidente Alberto Fernández.

En cuanto a las Comisiones, se realizaron un total de 323 reuniones durante el período que va del 10 de diciembre de 2019 a la misma fecha de este año. De ese total, 271 fueron reuniones de comisiones permanentes y 52 corresponden a comisiones especiales.

Las reuniones de trabajo, tanto de sesiones como de comisiones, se desarrollaron en esta nueva etapa con mayor asiduidad. Anteriormente, el mayor caudal de la actividad legislativa se situaba los días martes y miércoles, debido a que dependía de los viajes de los legisladores a la Ciudad de Buenos Aires.

Pero durante 2020, en cambio, se realizaron encuentros durante todos los días de la semana, dispuestas de forma correlativas y transmitidas en directo. Siempre monitoreadas por los equipos técnicos, administrativos y parlamentarios de la Cámara.

A su vez, se incrementó la participación activa de los legisladores en las reuniones, ya que no se vio afectada por las distancias. El trabajo remoto también se complementó con el registro de firma digital de más del 80% de los diputados que dictaminaron a distancia.

La Cámara baja también actúo como una caja de resonancia de la búsqueda de soluciones comunes. Como una muestra del trabajo junto al Poder Ejecutivo y las organizaciones sociales, hubo un total de 718 invitados a las reuniones de Comisión: 536 de la sociedad civil y 182 funcionarios, entre los que se cuentan a ministros, secretarios nacionales, provinciales, municipales y parlamentarios del Parlasur.

Entre las normas sancionadas, se aprobó la Sostenibilidad de la Deuda, que permitió llevar adelante una de las más exitosas reestructuraciones de la historia. También el Presupuesto nacional 2021, con eje central en el aumento de gastos de capital para realizar obras públicas. En materia sanitaria, se autorizó al gobierno a la compra de vacunas para generar inmunidad contra el Coronavirus y se aprobó el Programa de Protección al Personal de Salud, también conocido como "Ley Silvio" en homenaje a Silvio Cufré, el primer trabajador de salud del país que murió en el marco de la lucha contra la pandemia. La media sanción dada al proyecto de ley sobre el aborto legal representó otra muestra de funcionamiento mixto (presencial remoto) exitoso.

La Cámara de Diputados también acompañó el proceso de funcionamiento virtual de la sociedad. En ese contexto, se aprobó el acceso a la educación a distancia para todos los niveles, la promoción de recetas médicas digitales, la regulación del teletrabajo y el nuevo régimen de economía del conocimiento.

De cara al próximo año, cabe destacar que muchas de las leyes sancionadas en 2020 fueron aprobadas gracias al consenso entre las diversas fuerzas políticas, priorizando el diálogo y el respeto a la diversidad de opiniones.

La actividad en el Parlamento tampoco se detendrá durante el verano 2020-2021, debido a la prórroga de las sesiones ordinarias hasta el 3 de enero y el posterior comienzo de las sesiones extraordinarias hasta el 28 de febrero.