+++
author = ""
date = 2020-09-14T19:43:48Z
description = "Cecilia Todesca Bocco estima una inflación \"más cerca de 30 que de 40%\" en 2020 y \"un poquitito\" más que 28% el año próximo."
image = "/images/0037839097.jpg"
image_webp = "/images/0037839097.jpg"
title = "La vicejefa de Gabinete anticipó que se prevé para 2021 un alza del PIB cercana a 5%"

+++
_Cecilia Todesca Bocco estima una inflación "más cerca de 30 que de 40%" en 2020 y "un poquitito" más que 28% el año próximo._

La vicejefa de Gabinete de Ministros, Cecilia Todesca Bocco, afirmó que la caída del PIB se ubicará "en torno de 10 a 12%" este año, con una recuperación cercana a 5% en 2021, a la vez que ratificó la vigencia de las restricciones cambiarias.

Por otro lado, **llamó a "bajar un cambio respecto del ruido político"** y consideró que las discusiones tendrán que transcurrir en un clima "bastante menos altisonante del que hoy se observa", luego de hacer referencia a las discusiones que tuvieron lugar en la Cámara baja en torno a la continuidad de las sesiones telemáticas.

En este sentido, en una entrevista publicada hoy por el diario Ámbito, la funcionaria puso el acento en que **las circunstancias actuales "son lo suficientemente difíciles tanto para las empresas como para las familias".**

"Hay gente que dice barbaridades que son inadmisibles, impugnaciones de tipo personal gravísimas. No estamos para eso porque los problemas son suficientemente desafiantes", manifestó Todesca Bocco en la entrevista con el matutino porteño.

En cuanto a la presentación del proyecto de Presupuesto 2021, mañana en el Congreso, Todesca anticipó que estima una inflación punta a punta "más cerca de 30 que de 40%" en 2020 y "un poquitito" más que 28% el año próximo.

Sobre las perspectivas de recuperación, sostuvo que tras las caídas de abril y mayo, "en junio y julio se pegó la vuelta hacia arriba y agosto sigue subiendo, pero no con la misma tendencia empinada, baja un poquito".

> #### **"Si la actividad comienza a recuperarse se darán dos efectos, habrá una búsqueda por parte de los empresarios de recuperar tasa de ganancia y también una disputa por parte de los trabajadores para recuperar parte del salario perdido"**

Alertó, luego, que "si la actividad comienza a recuperarse se darán dos efectos, habrá una búsqueda por parte de los empresarios de recuperar tasa de ganancia y también una disputa por parte de los trabajadores para recuperar parte del salario perdido".

**"Necesitamos recuperar la masa salarial, pero no lo podemos hacer de un plumazo si queremos que sea una recuperación real, porque es justo y también porque, si el salario se recupera, lo hace también el consumo, que representa más de 70% del PBI",** resaltó.

Por otro lado, Todesca Bocco confirmó que se analizará "el ATP 6, que va tener la misma lógica de los dos últimos", es decir "continuar asistiendo con salario complementario a aquellas empresas que muestran una caída muy fuerte en su facturación".

Además, la vicejefa de Gabinete indicó que, en el caso del ATP 5, hubo 135.000 empresas a las que el Estado abonó el salario complementario de 1,4 millones de trabajadores y 26.560 empresas, más o menos, que pidieron el préstamo por 271.000 empleados.

**Sobre la continuidad del IFE (Ingreso Familiar de Emergencia), Todesca Bocco anticipó que están a la espera de "los datos sanitarios" porque, mientras ese aspecto no mejore, no se dejará a nadie "sin un acompañamiento".**

Por otra parte, admitió que "hay un diálogo abierto" con los productores de granos para que aceleren la liquidación de divisas, pero evaluó que "por lo pronto" allí "no" ven "un cambio muy fuerte en el corto plazo".

Explicó que **"si el productor no necesita los pesos, se queda con los granos y espera para venderlos, sobre todo porque algunos precios están subiendo".**

La vicejefa de Gabinete añadió que "vamos a continuar tratando de defender este cepo (cambiario), así como está", y descartó la posibilidad de un desdoblamiento cambiario así como también mayores restricciones a la compra de dólares".