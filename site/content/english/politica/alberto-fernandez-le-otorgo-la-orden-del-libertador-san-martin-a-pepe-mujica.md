+++
author = ""
date = 2021-01-25T03:00:00Z
description = ""
image = "/images/alberto-y-mujica.jpg"
image_webp = "/images/alberto-y-mujica.jpg"
title = "Alberto Fernandez le otorgó la Orden del Libertador San Martín a Pepe Mujica"

+++

**_Se trata de un reconocimiento que el Estado argentino les otorga “a los funcionarios, civiles o militares, extranjeros que en el ejercicio de sus funciones, merezcan en alto grado el honor y reconocimiento de la Nación”_**

El Gobierno de la República Argentina decidió condecorar al ex presidente de Uruguay José “Pepe” Mujica con el Collar de la Orden del Libertador San Martín, una distinción que el Estado les otorga “a los funcionarios, civiles o militares, extranjeros que en el ejercicio de sus funciones, merezcan en alto grado el honor y reconocimiento de la Nación”.

La medida se hizo efectiva este lunes, a partir de la publicación en el Boletín Oficial del Decreto 40/2021, en el que se explicó que este reconocimiento hacia el ex mandatario uruguayo “se basa en su vasta trayectoria y en su conducta ejemplar durante su vida pública”.

Entre los considerandos, el documento señaló que, a lo largo de su carrera, el dirigente del Frente Amplio supo estar al mando del Poder Ejecutivo de su país (entre los años 2010 y 2015), pero también fue ministro, diputado y senador. De hecho, el año pasado renunció a su banca en la Cámara alta para retirarse definitivamente.

“Más allá de los importantes cargos que ejerció, José “Pepe” Mujica consagró su vida a la militancia social y política por las causas populares de América Latina, y centró su actividad priorizando el bienestar de los sectores más postergados de la sociedad, con la convicción de que la política es una herramienta para mejorar sus vidas”, destacó el gobierno argentino.

En este sentido, en el Decreto las autoridades nacionales remarcaron que “los valores que rigen su vida personal son los mismos que enmarcaron su actividad en la función pública, caracterizada por la sobriedad y la decencia, por su estilo marcadamente austero y poco apegado a los protocolos innecesarios y suntuosos”.

“Desde muy joven se involucró activamente en la militancia social y política, por la que fue detenido junto con otros compañeros y con otras compañeras y enviado a la cárcel por más de una década durante la dictadura cívico militar que gobernó de facto la República Oriental del Uruguay entre los años 1973 y 1985″, se recordó.

Ya con el regreso de la democracia en su país, Mujica “se reincorporó a la actividad política y retomó su participación militante, trabajando por la construcción de mayorías que pudieran llegar al gobierno por vías democráticas para mejorar la vida del pueblo uruguayo”.

Sobre su presidencia, la Argentina destacó que el ex mandatario “puso como prioridad de su gestión la lucha contra la indigencia y la pobreza y promulgó las leyes de legalización del aborto y de matrimonio igualitario”, además de reconocer públicamente la responsabilidad del Estado “en las violaciones a los derechos humanos durante la dictadura, un gesto de innegable relevancia para la fortaleza ética de la República”.

“José ‘Pepe’ Mujica siempre fue fiel a sus valores y creencias, tanto en el ámbito privado como en la función pública, y se destacó por su fortaleza, su autoridad moral y su decencia. El 20 de octubre de 2020 renunció a su banca, pero siguió viviendo bajo los mismos valores que lo guiaron toda su vida”, ponderó el Decreto.

En concreto, el Gobierno argentino les otorga la Orden del Libertador San Martín a los funcionarios civiles o militares extranjeros que en el ejercicio de las funciones merezcan en alto grado el honor y reconocimiento de la Nación. Puntualmente, el nivel de “Collar” es el más alto que existe actualmente y está reservado para los soberanos o jefes de Estado.

Durante el Gobierno de Mauricio Macri, la Argentina les ha otorgado también esta misma condecoración a figuras internacionales tales como el presidente de China, Xi Jinping, el ex mandatario de Estados Unidos Jimmy Carter y el rey de España, Felipe VI.

Por el contrario, en agosto de 2017 la administración de Cambiemos decidió retirarle la Orden al dictador venezolano, Nicolás Maduro, debido a la violación sistemática de su régimen a los derechos humanos en el país caribeño. La condecoración le había sido entregada en mayo de 2013 por la entonces presidente, Cristina Kirchner.

En el último tiempo, también recibieron distintas distinciones varios embajadores extranjeros en el país: al entonces representante de Colombia, Luis Fernando Londoño Capurro, con la Orden de Mayo al mérito en el grado de “Gran Cruz”; a la enviada de Dinamarca, Grete Sillasen, con la Orden del Libertador San Martín también en el grado de “Gran Cruz”. Además, fueron condecorados los embajadores de Croacia, China, los Países Bajos, Australia y Kuwait.