+++
author = ""
date = 2021-02-14T03:00:00Z
description = ""
image = "/images/eumkkm6wyaqatrk.jpg"
image_webp = "/images/eumkkm6wyaqatrk.jpg"
title = "Alberto Fernández decreta 3 días de duelo por la muerte de Menem"

+++

**_El presidente Alberto Fernández decretó tres días de duelo nacional a partir de este domingo con motivo del fallecimiento de Carlos Saúl Menem, quien fue dos veces elegido presidente de la Nación._**

En ese marco, el mandatario expresó su "profundo pesar" por la muerte del senador riojano.

> "Con profundo pesar supe de la muerte de Carlos Saúl Menem. Siempre elegido en democracia, fue gobernador de La Rioja, Presidente de la Nación y Senador nacional", expresó en Twitter.

El Presidente destacó que Menem "en dictadura fue perseguido y encarcelado".

En la publicación expresó sus condolencias a la familia: "Vaya todo mi cariño a Zulema, a Zulemita y a todos los que hoy lo lloran".

#### El diálogo con Zulemita

Posteriormente, en un reportaje con Radio 10, Fernández relató que esta mañana, apenas falleció Menem, su hija Zulemita lo llamó para ponerlo al tanto, y él enseguida se comunicó con la vicepresidenta Cristina Fernández de Kirchner para hacer los arreglos de su velatorio en el Senado.

"Me llamó Zulemita cuando hacía diez minutos que su padre había fallecido, y hablé con Cristina para que organicemos las cosas", contó.

Fernández confirmó entonces que los restos del exmandatario serán velados en el Congreso, como se estila con los exmandatarios.

#### El recuerdo de Alberto Fernández

En la nota radial, Fernández relató cómo lo conoció, dijo que la historia seguramente dirá que tuvieron "miradas distintas" pero destacó "su naturaleza democrática" y lo calificó como un hombre "muy cariñoso y muy afectuoso" en el trato personal.

Recordó largamente a Menem. "Lo conocí cuando militábamos con Antonio Cafiero en su contra", explicó.

"Tenía una enorme fortaleza en el trato personal, en tratar al otro con afecto, con cariño, aun en las diferencias", dijo.

Fernández aseguró que "no le entraba en la cabeza" en su momento que Menem les hubiera ganado en la interna partidaria del Partido Justicialista.

"Veíamos en Menem una lógica de caudillo que no nos gustaba, pero la historia hizo que termine siendo Superintendente de Seguros del gobierno de Carlos Menem, y en esa función estuvo 5 años hasta que se resolvió la reforma constitucional", sostuvo el mandatario.

"Lo que más rescato de él fue su naturaleza democrática, ocupó todos los cargos por el voto popular", recordó Fernández.

También aseveró que "cuando la democracia desapareció los dictadores lo encarcelaron", y lo describió como "un hombre profundamente comprometido con la democracia y respetuoso del pensamiento ajeno".

"Tuve muchas diferencias y seguramente la historia escribirá que estuvimos siempre en veredas enfrentadas, con miradas distintas, pero esto no me hace perder de vista su trascendencia histórica", dijo Fernández.

También consideró que "fue un gran demócrata", añadió que "hay críticas que hacerle y la historia se ocupará" de ello, "pero nadie puede decir que en los años de Menem fue perseguido, encarcelado, o callado por lo que decía".

Siguiendo con su relato, recordó que "el Menem que se presentaba en campaña era un caudillo y nacionalista" e incluso había "inventado el Menem móvil", un vehículo blindado en el cual se movía saludando a la gente.

"Estaba haciendo lo que mejor sabía hacer, que era mirar a los otros a los ojos", definió sobre esas recorridas por el país que le dieron la victoria electoral en 1989.

El Presidente también contó una anécdota sobre lo impactada que quedó su madre cuando lo conoció. "Mi madre murió diciendo que Menem era una hombre maravilloso porque lo vio en en su mirada", explicó.