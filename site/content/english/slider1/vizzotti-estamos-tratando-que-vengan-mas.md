+++
author = ""
date = 2021-03-22T03:00:00Z
description = ""
image = "/images/maxresdefault-2-1-e1608302026503-1.jpg"
image_webp = "/images/maxresdefault-2-1-e1608302026503.jpg"
title = "VIZZOTTI: ESTAMOS TRATANDO QUE VENGAN MÁS"

+++

**_La ministra de Salud, Carla Vizzotti, adelantó que "en abril" estarían llegando al país las vacunas contra el coronavirus de AstraZéneca, que se suman a las que se están aplicando en Argentina, y ratificó la voluntad del Gobierno nacional de "vacunar a la mayor cantidad de gente" posible hasta que llegue la segunda ola, para bajar los niveles de mortalidad, incluso si hubiera que diferir la aplicación de la segunda dosis._**

"Estamos tratando que vengan más vacunas lo antes posible porque si logramos vacunar a la mayor cantidad de personas en riesgo de morir, con una dosis, esto tendría impacto directo en la baja de la mortalidad", dijo en diálogo con C5N.

Agregó que "durante abril estaríamos recibiendo dosis de AstraZéneca", aunque dejó claro que aún no se sabé "cuántas" y admitió que "si hubiéramos dependido de la producción europea o de la India estaríamos más atrasados".

Explicó también que la posibilidad de diferir la aplicación de la segunda dosis de la vacuna más allá de los 21 días mínimos que deben existir entre una y otra, es algo que quedó demostrado en Reino Unido, donde se pospuso hasta septiembre la aplicación de la segunda dosis y así "se generó una baja en los niveles de mortalidad".

Agregó que además del Reino Unido, donde se vacunaron 17 millones de personas con la primera dosis y sólo el 3 por ciento de ellas con las dos, se pospuso también el lapso entre la primera y segunda aplicación en Canadá, llevando el período a cuatro meses.

En ese sentido, dijo que "poder diferirlas lo más posible busca captar lo antes posible a la mayor cantidad de personas" y dejó claro que el período de 21 días es "el lapso mínimo" y no máximo.

Adelantó que el lunes se reunirá con los expertos y el jueves será el turno de la comisión nacional, al tiempo que reconoció que la pandemia generó una situación "tan dinámica" que día a día "se están analizando las situaciones" que se generan.

En este marco, reiteró que no se alienta el turismo y que las fronteras están cerradas.

También dejó claro que las cantidades de dosis que llegan al país no se conocen "hasta que no están embarcadas" porque "la provisión de vacunas es dinámica".

"Nosotros vamos a saber cuántas dosis han podido embarcar cuando cierren la puerta del avión y el avión sale. Por eso informamos cuando tenemos la certeza de la cantidad", consignó.

Admitió que ha cambiado la situación respecto al momento en el que "se firmaron los contratos y se presentaron cronogramas" y dijo que esa situación es dinámica y extensiva a todos los países, con diferencia entre la "teoría" que implica el momento en que se firmaron acuerdos con la "práctica", que es cuando se concreta la provisión de las dosis.

Entre "la teoría y la práctica, han surgido infinidad de inconvenientes que alargaron los tiempos".

Al ser consultada por la realización de las elecciones primarias abiertas, simultáneas y obligatorias (PASO), explicó que "cualquier decisión que se tome" teniendo en cuenta la situación de "excepcionalidad" que genera la pandemia, será la adecuada.

También dijo que la fabricación de vacunas y su provisión "es una situación totalmente extraordinaria desde todo punto de vista" debido a la escalada de la producción que se requiere para satisfacer la demanda mundial, con vacunas aún en desarrollo y la pandemia a pleno.