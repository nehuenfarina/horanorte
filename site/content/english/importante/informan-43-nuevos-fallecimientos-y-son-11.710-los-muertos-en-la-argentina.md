+++
author = ""
date = 2021-02-01T03:00:00Z
description = "En el reporte oficial se indicó, además, que fueron 9.909 los positivos de Covid-19 que se reportaron ayer en la Argentina y 565.446 el total de infectados hasta el momento, con un promedio diario en los últimos siete días de 11.063."
image = "/images/5fd5f167bd156_1004x565.jpg"
image_webp = "/images/5fd5f167bd156_1004x565.jpg"
title = "Murieron 43 personas y hubo 4.975 contagios de coronavirus en las últimas 24 horas"

+++
###### Con lo reportado este domingo los decesos desde el inicio de la pandemia ascienden a 47.974 y los infectados a 1.927.239.

Otras 43 personas fallecieron y 4.975 resultaron contagiadas de coronavirus en las últimas 24 horas en el país, con lo que los decesos desde el inicio de la pandemia son 47.974 y los infectados 1.927.239, de los cuales 1.721.650 son pacientes recuperados.

Los casos confirmados activos son 157.615, según el reporte emitido este domingo por el Ministerio de Salud.

El reporte agrega que 3.617 personas permanecen internadas en Unidades de Terapia Intensiva (UTI), y que la ocupación total de camas UTI a nivel nacional llega al 54,5 por ciento y en el Área Metropolitana de Buenos Aires (AMBA) al 60,7 por ciento.

De los 43 fallecimientos consignados por el reporte, 25 son hombres y 18 mujeres.

Entre los hombres, 11 eran residentes de la provincia de Buenos Aires, 1 en la Ciudad de Buenos Aires, 1 en Entre Ríos, 1 en Jujuy, 2 en Mendoza, 1 en Neuquén, 2 en Salta, 5 en Santa Fe y 1 en Tierra del Fuego.

Entre las mujeres, 2 eran residentes en la provincia de Buenos Aires, 1 en laCiudad de Buenos Aires, 1 en Córdoba, 4 en Entre Ríos, 1 en La Pampa, 1 en Mendoza, 1 en Neuquén, 1 en Río Negro, 1 en Salta, 3 en Santa Fe, 1 en Santiago del Estero y 1 en Tierra del Fuego.

En tanto, del total de contagios registrados en las últimas 24 horas, Buenos Aires reportó 2.227, la Ciudad de Buenos Aires 757; Catamarca 142; Chaco 86; Chubut 90; Corrientes 41; Córdoba 344; Entre Ríos 127; Jujuy 17; La Pampa 49; La Rioja 16; Mendoza 45; Misiones 132; Neuquén 253; Río Negro 74; Salta 28; San Juan 11; San Luis 22; Santa Cruz 97; Santa Fe 217; Santiago del Estero 88; Tierra del Fuego 41 y Tucumán 71.Formosa no registró nuevos casos.

La cifra de Tierra del Fuego incluye 13 casos en las Islas Malvinas según información de prensa (debido a la ocupación ilegal del Reino Unido, Gran Bretaña e Irlanda del Norte no es posible contar con información propia sobre el impacto del Covid-19 en esa parte del territorio argentino).

El Ministerio agrega que se realizaron 34.412 testeos en las últimas 24 horas y desde el inicio del brote 6.191.638, lo que equivale a 136.449 muestras por millón de habitantes.

La cartera de Salud informó ayer que, a partir de febrero, el Reporte Epidemiológico Covid-19 (REC) se emitirá una vez por semana, los martes a las 11.30 horas, desde la Sala de Situación.

#### **PANORAMA NACIONAL**

  
En este contexto, el ministro de Salud, Ginés González García, expondrá el miércoles próximo en la Cámara de Diputados sobre el plan de vacunación que está instrumentando el Gobierno contra el coronavirus y la política sanitaria para afrontar la pandemia.  
  
Será en el marco de una reunión de la comisión de Salud, presidida por el diputado del Frente de Todos Pablo Yedlin, a las 11 horas por videoconferencia y será la primera actividad del período de sesiones ordinarias que comenzó el 4 de enero último.  
  
La exposición de González García se producirá luego de que el jueves pasado arribara al país desde Moscú el tercer avión con 220 mil dosis de la vacuna rusa Sputnik V, que se distribuirán mañana en las 23 provincias y la ciudad de Buenos Aires.  
  
Hasta el momento, fueron vacunadas 278.451 personas, la mayoría de ellas pertenecientes al personal de salud, con la primera dosis y 65.583 con la segunda, según datos oficiales recabados hasta el 29 de enero.  
  
Por su parte, la asesora presidencial Cecilia Nicolini estimó que el grueso de las vacunas desarrolladas en Rusia contra el coronavirus llegará al país durante febrero.  
  
"Existe un retraso de dos o tres semanas" en la entrega de las dosis, agregó, mientras en Moscú están ampliando "la capacidad de producción porque realmente los resultados de la Sputnik V han sido buenísimos".  
  
En un reportaje con el portal El Cohete a la Luna, Nicolini dijo que a pesar "de las demoras, el acuerdo nos garantizó que sigan llegando vacunas para vacunar. Y cuando esa demora esté resuelta podremos recuperar el cronograma inicial, con el objetivo de que lleguen vacunas de manera considerable. Hoy esperamos que a mediados de febrero eso ya esté solventado".  
  
Nicolini, quien participó de las negociaciones para adquirir las vacunas, señaló que el acuerdo firmado con el Fondo de Inversión Directa de Rusia prevé la adquisición "de un total de 15 millones de vacunas, que son 30 millones de dosis, para ser entregadas de diciembre a marzo".

#### **PANORAMA MUNDIAL**

  
A nivel mundial, en las últimas 24 horas se detectaron 508.757 contagios y 13.433 muertes, lo que elevó los totales a 102,8 millones y más de 2,22, respectivamente, según la base de datos de la Universidad Johns Hopkins.  
  
En Europa, el temor a un colapso sanitario continuaba creciendo en Portugal, país con el mayor promedio móvil semanal de contagios y muertes del mundo, según el rastreador de datos www.ourworldindata.org, y tras registrar ayer 12.435 y 293, respectivamente.  
  
El Gobierno portugués pidió ayuda a Alemania, mientras que el canciller austríacos Sebastian Kurz comunicó que ofreció a Portugal tratar a sus pacientes graves.  
  
En Italia, el Ministerio de Salud informó que desde mañana habrá una relajación de las restricciones en 16 regiones, entre ellas Lazio (incluida la capital, Roma) y Lombardía, donde se encuentra Milán, centro financiero del país.  
  
En simultáneo, Bélgica impuso un toque de queda nocturno y la policía detuvo al menos a 200 personas que realizaban una protesta no autorizada frente a la Estación Central de Bruselas.  
  
En el continente americano, diez regiones de Perú, entre ellas Lima y la vecina El Callao, entraron en cuarentena estricta por dos semanas, en medio de un repunte de los contagios de coronavirus, escasez de oxígeno medicinal y demoras en la llegada de vacunas al país.  
  
En Estados Unidos, el país más enlutado con casi 440.000 muertos, los Centros para el Control y Prevención de Enfermedades emitieron una orden generalizada para exigir el uso de barbijo en el transporte público.  
  
Por su parte, las autoridades del estado de Australia Occidental declararon cinco días de confinamiento en las zonas de Perth, Peel y del suroeste del país, después de detectar que un guardia de seguridad de un hotel habilitado para la cuarentena contrajera coronavirus, el primero de transmisión comunitaria registrado en el estado en casi diez meses.  
  
China, en tanto, decidió prohibir temporalmente la entrada de extranjeros procedentes de Canadá, después que Ottawa, que suspendió sus vuelos con México y el Caribe, anunciara que los viajeros que entren en su suelo deben realizar un test PCR y aislarse en hoteles como mínimo tres días.  
  
En Israel, tras repetidas presiones de la ONU, el país con la tasa de vacunación más avanzada del mundo, informó que entregará 5.000 dosis contra la Covid-19 a la Autoridad Nacional Palestina para que inmunice a los médicos de los territorios ocupados que aún no recibieron ni una dosis.