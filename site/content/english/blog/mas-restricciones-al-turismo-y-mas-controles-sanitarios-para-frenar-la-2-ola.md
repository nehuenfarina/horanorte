+++
author = ""
date = 2021-03-22T03:00:00Z
description = ""
image = "/images/izvav2dg2jdo7cnate7q6vqkn4.jpg"
image_webp = "/images/izvav2dg2jdo7cnate7q6vqkn4.jpg"
title = "MÁS RESTRICCIONES AL TURISMO Y MÁS CONTROLES SANITARIOS PARA FRENAR LA 2° OLA"

+++
**_“Básicamente lo que queremos restringir es el turismo al exterior. Si te vas, te va a costar entrar”, confió Alberto Fernández en la quinta de Olivos cuando le preguntaron sobre la premisa sanitaria del próximo Decreto de Necesidad y Urgencia (DNU) que firmaría mañana para aplacar las eventuales consecuencias mortales de la segunda ola del COVID-19._**

El Presidente asume que las nuevas cepas del COVID-19 -Manaos y Sudafricana- pueden causar una nueva tragedia en la Argentina y decidió limitar los viajes turísticos al exterior. Alberto Fernández maneja información reservada que asegura que la mayoría de los argentinos viajan a Estados Unidos para aplicarse la vacuna.

Y no quiere correr riesgo con esos turistas locales que pueden actuar como vectores de las nuevas cepas.

Por eso, si no cambia de opinión en las próximas horas, el jefe de Estado dispondrá que los argentinos que regresan del exterior pagarán su examen de PCR y su cuarentena obligatoria en ciertos hoteles de la Capital Federal.

La ministra de Salud, Carla Vizzotti, debe definir la extensión de la cuarentena obligatoria. Hasta anoche, oscilaba entre 7 y 10 días corridos. Y su cumplimiento sería en hoteles porteños, aunque los turistas argentinos puedan probar que sus domicilios están en el interior del país.

¿Qué sucede con los argentinos que viajan al exterior para trabajar?-, le preguntaron a Alberto Fernández.

Esos deben justificar su viaje-, contestó lacónico.

La justificación del viaje no evita la cuarentena obligatoria. Sólo serviría para ahorrarse la estadía en un hotel de la Capital Federal. “El eventual contagio también puede suceder si te fuiste a trabajar, por eso todos los argentinos que lleguen del exterior irían a cuarentena”, reveló un integrante del Gabinete que ayer trabajaba en el DNU que Alberto Fernández firmaría mañana en la Casa Rosada.

Es decir: no importará si se viajó como turista o por razones laborales. Y tampoco servirá probar que fue vacunado o que tiene muchísimos anticuerpos. En todos los casos, la cuarentena sería obligatoria en los hoteles porteños para los argentinos que regresaron del exterior.

Al margen del pago de la cuarentena obligatoria y del PCR para los argentinos que vuelan al exterior, el presidente define una estrategia de control exhaustivo en las fronteras del país. Su preocupación es Brasil, Paraguay y Bolivia -en ese orden-, y por eso cerca del mediodía habrá un zoom colectivo entre los gobernadores con pasos limítrofes y los ministros de Salud, Interior, Seguridad y Transporte.

El gobierno analiza desplegar fuerzas de seguridad y pedir el apoyo logístico de las Fuerzas Armadas. Las fronteras en el Norte son porosas y la Gendarmería Nacional puede hacer muy poco para evitar el paso clandestino desde Brasil, Paraguay y Bolivia. Se trata de un asunto delicado: cientos de argentinos compran y venden a través de las fronteras y es su único modo de subsistencia.

Alberto Fernández intenta un delicado equilibrio entre el control sanitario y la crisis económica. Si en las fronteras todo se reduce a evitar la circulación informal entre la Argentina, Brasil, Paraguay y Bolivia, puede ocurrir una sucesión eventos espontáneos de desobediencia civil.

Además de los controles fronterizos, el presidente ordenó a su gabinete que entable una nueva conversación con los gobernadores para reforzar el seguimiento diario de los argentinos que regresaron del exterior y hacen la cuarentena obligatoria en sus provincias.

Los ministros Vizzotti (Salud) y Eduardo “Wado” de Pedro (Interior) tienen probado que se relajaron los controles en las provincias y que la cuarentena es una obligación penal de escaso cumplimiento. Alberto Fernández pretende que se refuercen los controles en el interior, mientras se intenta desalentar el turismo exterior para aplacar las eventuales consecuencias mortales de la segunda ola del COVID-19.

La Casa Rosada hoy será escenario de un desfile continuo de ministros y secretarios para definir junto al jefe de Estado todas las medidas que se implementaran para evitar que la pandemia profundice la tragedia social y sanitaria en la Argentina.

Si la interna política y la burocracia estatal no traba su redacción final, Alberto Fernández firmará mañana el DNU contra la segunda ola del COVID-19.