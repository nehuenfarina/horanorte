+++
author = ""
date = 2021-03-22T03:00:00Z
description = ""
image = "/images/clase-media.png"
image_webp = "/images/clase-media.png"
title = "UNA CLASE MEDIA DEVASTADA DURANTE LA ÚLTIMA DÉCADA"

+++

**_La clase media está cada día más empobrecida. La conclusión, aunque parece obvia, no deja de sorprender cuando se observan los datos oficiales y sobre todo la tendencia que no encuentra freno, con o sin pandemia, con o sin cuarentena, con un espacio u otro en el poder político._**

Un trabajo de la consultora Abeceb destaca que “los límites entre la clase media, la población más vulnerable y aquella considerada rica no están estrictamente definidos. La pertenencia a la clase media no depende exclusivamente del nivel de ingresos sino que surge de la combinación de la evolución de variables de ingreso, educativas, de inserción laboral, valores, creencias y estilo de vida, que entre otras cosas reflejan status, como el acceso a determinados bienes aspiracionales”, expresó en este informe.

Con este fin, Abeceb, fundada por el economista y ex ministro de Producción en el gobierno de Cambiemos, Dante Sica, analizó una serie de indicadores que reflejan el “empobrecimiento” de la clase media, algo que se refleja en los últimos datos del Indec y de la Dirección de Estadística porteña.

En todas esas dimensiones, que corresponden a la clase media”. Al respecto, concluyeron que:

El peso de la clase media en el total de la población muestra un retroceso importante. “Si analizamos la evolución de la clase media en base a sus ingresos declarados, observamos que desde 2015, el peso de la clase media en CABA se redujo en alrededor de 8 puntos, lo que acompañado por un aumento de la pobreza se vincula a un proceso de empobrecimiento, que se refleja en una mayor porción de hogares vulnerables, pobres e indigentes”.

Aunque los datos corresponden a la Ciudad de Buenos Aires, se aclaró que “el deterioro de los ingresos y la inserción laboral ha sido generalizado en todas las provincias”. Además, “la distribución de los hogares por nivel socioeconómico también se deterioró, afectando a la clase media”.

“Teniendo en cuenta el nivel educativo, el tipo de inserción laboral y cuestiones vinculadas a la organización familiar y la vivienda, el peso de la clase media sobre el total de los hogares también cayó”, destacó.

Mientras que en 2014 “el 48% de los hogares podía ser considerado de clase media (media-media o media-baja), en 2019 el 45% de los hogares pertenecía a ese estrato, lo que estuvo acompañado por un aumento de los hogares en un estrato bajo”, durante el gobierno de Mauricio Macri.

Por otro lado, “el poder de compra de los ingresos muestra un marcado deterioro en la capacidad de compra de bienes durables, que se asocian a las canastas de consumo o bienes aspiracionales vinculados usualmente a la clase media”.

En este caso, se incluye la posibilidad de comprar un auto, una casa propia o acceder a electrodomésticos y electrónica de consumo.

“Con precios total o parcialmente dolarizados hoy se necesitan cerca de dos años de salarios promedio para comprar un auto, lejos de los 14 sueldos promedio que se necesitaban en 2013”.

Además, “alcanzar una propiedad en línea con el valor promedio de escrituración requirió en promedio en 2020 de 132 sueldos promedio”. A su vez, “el poder de compra de una canasta de electrodomésticos cayó un 20% en un año”.

La autora del informe, Belén Rubio, dijo que “del trabajo se desprenden tres conclusiones:

El empobrecimiento de la clase media es el reflejo de la crisis de crecimiento y confianza que lleva adelante Argentina desde más de una década.

Este proceso contribuye a limitar las perspectivas y anhelos de aquellas familias que apuestan a la educación y al trabajo como motor de crecimiento, y que desalienta la sensación de progreso necesaria para emprender y apostar por el país, incluso impulsando la migración.

Contribuye a dificultar las bases del ahorro, del sistema de seguridad social usualmente reflejado en el trabajo formal y de los sistemas de protección intergeneracionales.

Décadas atrás, “una amplia clase media con acceso a servicios básicos, a salud y a educación era lo que diferenciaba a la Argentina del resto de los países latinoamericanos y contribuía a una sensación de progreso. Hoy esa población quedó desprotegida y se ubica entre aquellos que tienen activos y se cubren mediante la dolarización y las propiedades, y otros que están protegidos por la asistencia social”.

“Termina siendo la clase que más paga los costos y los ajustes, y encima, está desprotegida ante el futuro”, concluyó Rubio.

Por lo visto, en términos del empobrecimiento de la clase media, no hay “grieta”: hay un proceso casi sin interrupciones y que no reconoce diferencias entre los diferentes partidos políticos.