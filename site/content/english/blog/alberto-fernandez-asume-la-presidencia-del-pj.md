+++
author = ""
date = 2021-03-22T03:00:00Z
description = ""
image = "/images/5d9dcbae42eb6_1004x565-1.jpg"
image_webp = "/images/5d9dcbae42eb6_1004x565.jpg"
title = "ALBERTO FERNÁNDEZ ASUME LA PRESIDENCIA DEL PJ"

+++

**_El presidente Alberto Fernández asume hoy la presidencia del Consejo del Partido Justicialista (PJ) a nivel nacional, en un acto que se realizará en el club Defensores de Belgrano y al que asistirán funcionarios, gobernadores, legisladores, intendentes, referentes sindicales y partidarios, aunque en un número limitado y bajo estrictos controles sanitarios por la pandemia de coronavirus._**

El proceso de renovación de autoridades del PJ nacional en un año electoral se completará hoy, a las 19, en el club Defensores de Belgrano -Comodoro Martín Rivadavia 1450-, en el barrio porteño de Núñez, con el primer encuentro del flamante consejo que conducirá los destinos partidarios en una etapa que se inicia con el peronismo en el poder y que tiene entre sus objetivos "modernizar el partido" y darle una estructura "más ágil", señalaron fuentes cercanas al primer mandatario.

Luego de casi cinco años de presidencia del diputado nacional José Luis Gioja en un contexto adverso por la división del peronismo tras la derrota del 2015 y la intervención judicial en 2018, ahora será el turno de Fernández, en una situación muy diferente y que cumple con una tradición no escrita que marca que cuando el justicialismo está en el Gobierno, el presidente de la Nación es el líder partidario.

Gioja, quien se despidió hoy con una carta abierta a la militancia, fue saludado por funcionarios y dirigentes que agradecieron su gestión al frente del partido.

"Imprescindible la tarea militante del PJ para garantizar la unidad del peronismo y la construcción del Frente De Todos", destacó el jefe de Gabinete, Santiago Cafiero, en un reconocimiento vía redes sociales al presidente saliente del justicialismo, mientras el ministro del Interior, Eduardo "Wado" De Pedro, definió a Gioja como "protagonista de la unidad" y destacó su figura por "saber llevar las riendas en momentos muy difíciles".

En su carta, Gioja repasó que, en el trabajo de la unidad partidaria, "primó la razón de los dirigentes para poner por delante los intereses de la Patria por sobre los personales", y consideró un "gran honor en lo personal y en lo político" que lo hayan elegido para presidir el partido en el que militó "toda su vida" y que le permitió "acceder a importantes lugares de conducción" como la gobernación de San Juan por tres períodos.

En el acto de asunción de mañana el Presidente estará acompañado por todos los sectores del peronismo que consensuaron la lista denominada "Unidad y Federalismo", que representa a los distintos espacios dentro del movimiento justicialista, incluidas agrupaciones como La Cámpora, que había quedado afuera del PJ en 2016.

Bajo la premisa de "unidad en la diversidad", el Consejo del PJ nacional estará integrado por la vicepresidenta 1°, la diputada nacional Cristina Álvarez Rodríguez; vicepresidente 2°, el gobernador bonaerense, Axel Kicillof; vicepresidenta 3°, la vicegobernadora de Chaco, Analía Rach Quiroga; vicepresidente 4°, el gobernador de Tucumán, Juan Manzur; la vicepresidente 5°, la diputada nacional Lucía Corpacci; cumpliendo con la ley de Paridad de Género.

También acompañarán a Alberto Fernández en el PJ los ministros Santiago Cafiero (Gabinete); Eduardo de Pedro (Interior) y Agustín Rossi (Defensa); los secretarios Julio Vitobello (General), Fernando Navarro (Relaciones Parlamentarias) y Guillermo Olivieri (Culto); y la titular de Anses, Fernanda Raverta.

Forman parte de esta nueva conducción los sindicalistas Héctor Daer (Sanidad), Pablo Moyano (Camioneros), Antonio Caló (UOM), Víctor Santa María (Suterh), Ricardo Pignanelli (Smata) y Hugo Yasky (CTA).

Además de Kicillof y Manzur, los gobernadores peronistas que acompañan a Fernández como consejeros en la conducción partidaria son Sergio Ziliotto (La Pampa), Sergio Uñac (San Juan), Ricardo Quintela (La Rioja), Gustavo Bordet (Entre Ríos), Omar Perotti (Santa Fe), Alicia Kirchner (Santa Cruz) y la vicegobernadora bonaerense Verónica Magario.