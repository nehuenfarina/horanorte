---
title: La Copa Libertadores vuelve después de seis meses y tres días de suspensión
date: 2020-09-14T20:30:12.000+00:00
author: ''
image_webp: "/images/201910240830377693f5dfa397a50f047e556d10211dde_med.jpg"
image: "/images/201910240830377693f5dfa397a50f047e556d10211dde_med.jpg"
description: Debido a la pandemia, el reglamento de la Libertadores permitió a los
  clubes ampliar la lista de buena fe de 30 a 50 jugadores para afrontar cualquier
  complicación por contagios.
categories: ["deportes"]
tags: ["libertadores","covid","boca", "copa libertadores"]
---
_Debido a la pandemia, el reglamento de la Libertadores permitió a los clubes ampliar la lista de buena fe de 30 a 50 jugadores para afrontar cualquier complicación por contagios._

La Copa Libertadores 2020 retomará mañana su competencia con 4 partidos de la tercera fecha de la fase de grupos después de una suspensión de seis meses y tres días por la pandemia de coronavirus.

La pelota volverá a rodar con estadios cerrados y bajo un protocolo especial que elaboró la Conmebol y fue aprobado por los gobiernos de los diez países de las asociaciones miembro de la entidad.

En momentos que el virus golpea fuerte en Sudamérica, con cuatro países (Brasil, Perú, Colombia y Argentina) entre los de mayor cantidad de contagios en el mundo, los equipos participantes llegan con distintos niveles de preparación y rodaje.

En ese sentido, River Plate, finalista de la última edición, Boca Juniors, Racing Club, Defensa y Justicia y Tigre, que se presentarán el jueves, tienen una clara desventaja.

Los clubes argentinos fueron autorizados para volver a los entrenamientos a principios de agosto y ninguno de ellos pudo realizar partidos amistosos por la evolución de la pandemia en el país.

La competencia se reanudará mañana desde las 19.15 con dos juegos del Grupo C: Jorge Wilstermann-Athletico Paranaense (Brasil) en Bolivia y Colo Colo-Peñarol (Uruguay) en Santiago de Chile.

Desde las 21.30, Binacional de Perú recibirá en Lima a la Liga Deportiva Universitaria de Quito (Ecuador) en un partido del Grupo D que lidera River por diferencia de gol.

Y a la misma hora, en Brasil, Santos será local de Olimpia de Paraguay en el Grupo G, integrado por Defensa y Justicia.

La actividad de los argentinos será el jueves de acuerdo al siguiente programa: Racing Club-Nacional de Uruguay a las 17; San Pablo-River y Defensa Justicia-Delfín de Ecuador desde las 19; Libertad de Paraguay-Boca Juniors a partir de las 21 y Guaraní de Paraguay-Tigre a las 23.

En cuatro meses y medio, la Copa Libertadores 2020 jugará los 93 partidos que restan para conocer al nuevo campeón: 64 de la segunda fase, 16 de octavos de final, 8 de cuartos, 4 de semifinales y la definición, prevista a partido único en el mítico Maracaná de Río de Janeiro.

Esta semana se jugará la tercera fecha de la ronda de grupos, la próxima (del martes 22 al jueves 24) será la cuarta jornada; la quinta tendrá lugar entre el 29 de septiembre y el 1 de octubre y la sexta y última, del 20 al 22 de octubre.

Los octavos de final, sorteados el 24 de octubre, se jugarán del 24 al 26 de noviembre (ida) y entre el 1 y 3 de diciembre (revanchas); los cuartos, del 8 al 11 de diciembre (ida) y del 15 al 17 del mismo mes (vuelta).

Las semifinales quedarán para el año próximo. Los primeros partidos se disputarán la semana del 5 de enero y los desquites la semana del 12. La final en Río de Janeiro aún no tiene fecha pero será del 20 al 30 de enero.

La Conmebol invirtió 95,5 millones de dólares para completar la presente edición de la Libertadores, cuyo último partido se disputó hace 187 días. La entidad rectora del fútbol sudamericana repartió ese monto a las federaciones nacionales para financiar gastos corrientes, de control médico y logística.

Para garantizar la seguridad sanitaria, cada equipo que viaje para la disputa de un partido en el exterior deberá hacerlo con una delegación que no supere las 50 personas y respetar un corredor sanitario que comprende el viaje, la estadía en el hotel, los desplazamientos internos y la actividad en el estadio el día del partido.

En el aspecto deportivo, de manera excepcional por la pandemia, el reglamento de la Libertadores permitió a los clubes ampliar la lista de buena fe de 30 a 50 jugadores para afrontar cualquier complicación por contagios.

Con el mismo espíritu, la Conmebol permitirá que un futbolista pueda jugar en más de un equipo durante la presente edición del torneo, algo que no estaba permitido, y habilitó que cada equipo pueda realizar hasta cinco cambios en un partido, del mismo modo que lo hizo la UEFA en la Champions League y la Liga de Europa.

. Programa de partidos de la tercera fecha de la fase de grupos:

= Martes 15 =

\- Grupo C -  
19\.00: Jorge Wilstermann (Bolivia)-Athletico Paranaense (Brasil), Ángelo Hermosilla (Chile).  
19\.00: Colo Colo (Chile)-Peñarol (Uruguay), Mauro Vigliano (Argentina). Fox Sports.

\- Grupo D -  
21\.30: Binacional (Perú)-LDU de Quito (Ecuador), Ivo Méndez (Bolivia). ESPN 2.

\- Grupo G -  
21:30: Santos (Brasil)-Olimpia (Paraguay), Leodán González (Uruguay). Fox Sports 2

= Miércoles 16 =

\- Grupo E -  
19\.15: Internacional (Brasil)-América de Cali (Colombia), Facundo Tello (Argentina). ESPN 2.  
21\.30: Universidad Católica (Chile)-Gremio (Brasil), Darío Herrera (Argentina). Fox Sports.

\- Grupo F -  
19\.15: Estudiantes de Mérida (Venezuela)-Alianza Lima (Perú), Nicolás Gallo (Colombia). Fox Sports 2.

\- Grupo B -  
21\.30: Bolívar (Bolvia)-Palmeiras (Brasil) Piero Maza (Chile). ESPN 2.

\- Grupo H -  
21\.30: Independiente Medellín (Colombia)-Caracas (Venezuela), Carlos Oribe (Ecuador).

= Jueves 17 =

\- Grupo F -  
17\.00: Racing Club-Nacional (Uruguay), Anderson Daronca (Brasil). Fox Sports.

\- Grupo G -  
19\.00: Defensa y Justicia-Delfín (Ecuador), Roberto Tobar (Chile). ESPN.

\- Grupo D -  
19\.00: San Pablo-River, Esteban Ostojich (Uruguay). ESPN 2.

\- Grupo H -  
21\.00: Libertad (Paraguay)-Boca, Rodolpho Toski (Brasil). ESPN 2.

\- Grupo A -  
21\.00: Independiente del Valle (Ecuador)-Flamengo (Brasil), Wilmar Roldán (Colombia). Fox Sports 2.  
23\.00: Barcelona (Ecuador)-Junior (Colombia), Kevin Ortega (Perú). ESPN.

\- Grupo B -  
23\.00: Guaraní (Paraguay)-Tigre, Gustavo Tejera (Uruguay). Fox Sports.

![](https://www.telam.com.ar/advf/imagenes/2020/09/5f5fcc41f19c2_1004x1802.jpg)