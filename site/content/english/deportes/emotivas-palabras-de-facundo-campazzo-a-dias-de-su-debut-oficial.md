+++
author = ""
date = 2020-12-21T17:00:00Z
description = ""
image = "/images/campazzo.jpg"
image_webp = ""
title = "Emotivas palabras de Facundo Campazzo a días de su debut oficial"

+++
**_El próximo miércoles será el debut del argentino en la NBA. En la previa, brindó una entrevista y habló de todas sus emociones._**

Facundo Campazzo arribó a los Denver Nuggets, desde el Real Madrid, para competir en la NBA. Pese a que ya disputó algunos minutos en partidos amistosos de pretemporada, este miércoles tendrá su debut oficial en la competencia frente a Sacramento Kings.

En una entrevista con el diario español As, el argentino manifestó: “De chico la NBA era una fantasía super lejana, pero de grande sí he soñado con llegar. Ya llegué, estoy acá y quiero jugar bien, no me conformo con debutar, intentaré hacerme un hueco. Debo ser como un buen estudiante que reúne toda la información posible y trabaja para ser mejor. Estoy ansioso, es cierto, algo nervioso incluso, siempre hay incertidumbre, aunque así fueron todos los cambios importantes en mi carrera y eso me ayuda a dar lo mejor de mí”.

“Creo que mi rol va a estar en imponerme desde la defensa, en los pick and roll defensivos, en los unos contra unos, o por lo menos yo voy a ir por ahí: aspectos defensivos, intensidad y demás. No voy a tener la pelota en la mano como en el Madrid o en anteriores clubes, así que intentaré mejorar el juego sin balón para poder coincidir en cancha con Jamal Murray o Monte Morris”, comentó en relación al funcionamiento deportivo del equipo y a su tarea en particular.

La salida de Campazzo del Real Madrid no fue sencilla, ya que era ídolo y figura. Sin embargo, reveló que habló con Florentino Pérez: “Me deseó suerte, me dijo que quería que me fuera bien, pero que supiera que el Madrid era mi casa, que me iban a esperar siempre”.

En último lugar, con respecto a sus objetivos en la NBA, remarcó: "Poder entrar en los playoffs y jugar por cosas importantes, medirme a los mejores. Con algunos lo hice en los Juegos y en los Mundiales. A (Stephen) Curry nunca me enfrenté, me ilusiona encontrarme con (Luka) Doncic después de haber vivido tan buenos momentos en el Madrid. Un jugador como él solo sale cada no sé cuántos años”.