+++
author = ""
date = 2020-12-20T04:00:00Z
description = ""
image = "/images/messi.jpg"
image_webp = ""
title = "Lionel Messi rompió el silencio a días de quedar libre con el Barcelona"

+++
**_El capitán y referente azulgrana habló acerca de cómo vivió los primeros meses después de anunciar que no quería continuar en el club._**

La situación que atraviesa Lionel Messi en el FC Barcelona mantiene a más de uno en vilo, después de que el propio futbolista anunció en agosto que quería hacer uso de la cláusula que le permitía salir del club como agente libre. Finalmente, se quedó para no entrar en conflictos judiciales con la institución.

El contrato lo une al cuadro azulgrana hasta junio del 2021, sin embargo, dentro de 11 días (a partir de enero) el astro argentino podrá empezar a escuchar ofertas y negociar con cualquier club que quiera tenerlo y, en el caso de convencerlo, podrá llegar gratis desde el 1 de julio.

Según indicaron del programa radial El Mon de RAC1, el rosarino no tomará una decisión hasta hablar con el nuevo presidente, el cual se conocerá con las votaciones de mediados de enero. En la misma sintonía, la emisora española reveló unas recientes declaraciones que hizo la Pulga en una entrevista exclusiva que saldrá a la luz el próximo domingo 27 de diciembre.

“La verdad es que hoy por hoy estoy bien. Es verdad que lo pasé muy mal en verano. Venía de antes. Lo que pasó antes del verano, por cómo terminó la temporada, el burofax y todo eso... Después lo arrastré un poco al comienzo de la temporada”, explicó Messi en una conversación que mantuvo con el periodista Jordi Évole.

“Hoy por hoy estoy bien y me encuentro con ganas de pelear en serio por todo lo que tenemos por delante. Me encuentro ilusionado. Sé que el club está pasando por un momento complicado, a nivel de club y de equipo, y se hace difícil todo lo que rodea el Barcelona, pero estoy con ganas”, fue otra de las declaraciones del capitán azulgrana.

Según detalló el programa radial catalán, con el correr de los días se irán conociendo nuevas frases de la entrevista, la cual se publicará de forma íntegra el domingo 27 de diciembre en el canal televisivo La Sexta.

El caso Messi es el tema que más preocupa a los candidatos a presidir el Barcelona. Desde la partida de Josep Maria Bartomeu, de nula relación con el argentino, es el tesorero del club Carles Tusquets el que se encuentra al mando hasta el próximo 24 de enero, representando a una junta gestora.

La mayoría de los postulados a la presidencia tienen un plan especial para convencer al argentino de continuar en la institución, algo que, hasta el día de hoy, parece algo complicado, por los resultados deportivos, la falta de un proyecto ganador y las cuestiones económicas y políticas que atraviesa el club.