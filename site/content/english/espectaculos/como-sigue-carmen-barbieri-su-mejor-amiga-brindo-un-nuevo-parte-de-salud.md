+++
author = ""
date = 2021-02-01T03:00:00Z
description = ""
image = "/images/carmen-y-su-amiga.jpg"
image_webp = "/images/carmen-y-su-amiga.jpg"
title = "¿Cómo sigue Carmen Barbieri?: su mejor amiga brindó un nuevo parte de salud"

+++

**_Sandra Domínguez en diálogo con Implacables contó cómo continúa la capocómica que se encuentra internada en terapia intensiva de la Clínica Zabala._**

Debido a que su cuadro de salud se agravó, este fin de semana, Carmen Barbieri fue inducida a un coma farmacológico. La noticia generó un gran revuelo mediático y muchos famosos salieron a darle su apoyo a la capocómica en las redes. Su hijo, Federico Bal, por su parte, les pidió a sus seguidores de Twitter que rezaran y mandaran lindas energías para ella para que tuviera una pronta recuperación.

Ahora, Sandra Domínguez en diálogo con Implacables, el programa que conduce Susana Roccasalvo en El Nueve, contó cómo continúa la capocómica que se encuentra en la Clínica Zabala. "Nunca mejor dicho ‘buenas noches’ porque tengo buenas novedades. Hay una leve mejoría en Carmen", señaló la actriz vía telefónica apenas salió al aire.

"Sigue sin fiebre, era un cuadro que ya estaba mejorando antes de entrar con el respirador. Ahora le sacaron los antibióticos porque dieron negativos todos los cultivos. Es muy importante", indicó.

> "La pusieron boca abajo anoche cuando no estaba haciendo una evolución y estaba medio mal. Y a esa cosa que empiezan a aprender con la práctica los doctores, se van sumando técnicas nuevas y va mejorando el cuadro", contó. "Mejoró absolutamente el cuadro y, de seguir bien, van a tratar de volverla a poner boca arriba a ver si ese cuadro que mejoró se sostiene", explicó.