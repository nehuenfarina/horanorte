+++
author = ""
date = 2020-09-14T20:00:00Z
description = "Acompañado por su banda completa, el intérprete regaló a sus seguidores una larga nota previa de 45 minutos realizada por la periodista anfitriona Eleonora Pérez Caressi y un concierto de unas 35 canciones de su amplio repertorio."
image = "/images/5f5e16e303871_1004x565-1.jpg"
image_webp = "/images/5f5e16e303871_1004x565.jpg"
title = "Unas 35 mil personas disfrutaron del extenso show por streaming de Abel Pintos"

+++
###### _El show hizo foco "en la canción propiamente dicha y no en todo lo que pasa alrededor”._

Unas 35 mil personas, entre las 20 mil que adquirieron sus tickets virtuales y las 15 mil que lo siguieron por Radio Nacional, disfrutaron anoche del extenso primer show por streaming ofrecido por Abel Pintos, desde el escenario de La Usina del Arte.

**Entre las canciones sonaron la flamante "Piedra libre", dedicada al hijo que espera; "El hechizo", "Quiero cantar", "Cuantas veces", "Como te extraño", "Aquí te espero", "Sin principio ni final", el clásico de León Gieco "Pensar en nada", y "A-dios", entre tantas.**

El artista estuvo acompañado por su director musical Marcelo Predacino, en guitarras y coros; Alfredo Hernández, en teclados y coros; Alan Ballan, en bajo y coros; Ervin Stutz, en trompeta; Carlos Arin, en saxo; y Jose Luis Belmonte, en batería y Ariel Pintos en guitarra y coros.

Tal como había anticipado el propio Pintos días atrás a Télam, el show hizo foco "en la canción propiamente dicha y no en todo lo que pasa alrededor”.

"**El streaming viene a ser un nuevo formato de concierto y no una manera de emular un concierto en vivo que es imposible de igualar y es una experiencia única, mágica y metafísica que sucede cuando compartimos todos juntos en un mismo lugar.** A este streaming no llego con pretensiones de show, tengo otros objetivos y busco provocar otras cosas. Ya de por sí estoy excitado con la posibilidad de volver a tocar con mis amigos, con mi banda”, había aclarado.

Por ese motivo, además de la experiencia de verlo tocar en vivo desde un escenario junto a su banda, **el músico ofreció la opción de poder seguir el concierto solo en audio por la radio pública.**