+++
author = ""
date = 2021-02-17T03:00:00Z
description = ""
image = "/images/boquete-en-rosario.jpg"
image_webp = "/images/boquete-en-rosario.jpg"
title = "Toman de rehén a una familia y abren un boquete en su casa para robar una empresa de caudales"

+++

**_Un grupo de ladrones tomó de rehén en la ciudad santafesina de Rosario a una familia y abrió un boquete en una pared de su vivienda, que es lindera a una empresa de transporte de caudales, de la que robaron una cifra aún no precisada de dinero, indicaron este martes fuentes policiales._**

Voceros del caso indicaron que “lo sustraído sería una gran cantidad de dinero, todavía no precisada”.

La familia que fue tomada de rehén no sufrió agresiones ni violencia de parte de los ladrones, según explicó una de las víctimas, a quien le quedó un enorme boquete en una pared de su residencia.

> “Nos hicieron un trabajo de inteligencia, se ve que conocían el movimiento de mi casa, porque se sorprendieron por la presencia de mi hija y de mi nieto”, dijo Guillermo Algañaraz (62), propietario de la vivienda.

Según contó esta mañana a la prensa local, Algañaraz, quien vive en la casa de pasillo junto a su hermano, anoche alrededor de las 21 recibió a su hija, que fue a visitarlo junto a su nieto.  
  
“La puerta estaba abierta porque estaba esperando a mi hija que nos venía a visitar con mi nieto. Se ve que conocían el lugar y que hicieron trabajos de inteligencia previo”, sostuvo el hombre, un docente jubilado.  
  
Explicó que detrás de su hija “se metieron los delincuentes” y agregó que los asaltantes “se sorprendieron cuando vieron” a su nieto, atento a que uno de ellos preguntó: “¿Por qué tanta gente?”.  
  
Según reconstruyeron los investigadores, dos hombres maniataron a las cuatro personas y los encerraron en una habitación, y mantuvieron una “guardia” sobre los rehenes.  
  
En tanto, otro grupo –que la policía estima en relación al testimonio de Algarañaz que podrían ser cinco personas- comenzaron a agujerear la pared lindera, que da al depósito de la empresa Brinks.  
  
Trabajaron con herramientas propias y también tomaron algunas que había en la casa, explicó Algarañaz.

Voceros del caso indicaron que Brinks posee personal de seguridad, aunque aparentemente no estaba por el feriado largo.  
  
El jubilado contó esta tarde al canal TN que los delincuentes lo esposaron a él “primero” y le dijeron que se quedara “tranquilo” ya que “el objetivo era la transportadora” y a su familia no le iban “a robar nada”, e incluso le dieron su billetera con los 2.000 pesos que guardaba dentro.  
  
“Todavía sigo con el susto propio, pero debo estar contento porque nos trataron bien, en ningún momento hubo agresión, ni siquiera intimidación porque lo que yo noté es que estaban armados pero siempre con los cañones apuntando para abajo”, relató el dueño de la propiedad.  
  
En ese sentido, señaló que los asaltantes estaban “tranquilos”, y fueron muy “profesionales”, al punto de que mientras toda su familia permanecía atada les “dieron agua todo el tiempo” y les pusieron “un ventilador” en la pieza porque se estaban “ahogando”.  
  
Por su parte, una vecina advirtió los ruidos durante la noche del lunes, por lo que gritó a su vecino si sucedía algo.  
  
Algarañaz contó que uno de los ladrones le pidió que le respondiera que estaban arreglando un caño, mientras el resto de la banda terminaba de abrir el boquete en la pared.  
  
Los pesquisas creen que los ladrones escaparon por el fondo del inmueble de la familia retenida, que da a unas vías del ferrocarril.  
  
“Nos dejaron atados, después que se fueron estuvimos más de media hora hasta que pudimos liberarnos”, contó el docente jubilado, para agregar que también les llevaron los teléfonos, con el fin de incomunicarlos.  
  
Finalmente Algarañaz logró salir de su casa con las manos atadas a la espalda y pidió ayuda a un taxista, que avisó a la Policía.