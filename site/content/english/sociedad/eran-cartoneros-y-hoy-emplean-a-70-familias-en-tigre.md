+++
author = ""
date = 2020-12-07T20:00:00Z
description = ""
image = "/images/ccc.jpg"
image_webp = ""
title = "Eran cartoneros y hoy emplean a 70 familias en Tigre"

+++
**_La cooperativa Creando Conciencia se creó en 2006. Recolectan, separan, clasifican y reciclan residuos. Tienen su propia planta y cuatro unidades de negocio._**

“Está muy bueno. Me gusta ver cómo cada uno crece. Muchos compañeros empezaron siendo separadores y se convirtieron en coordinadores. Algunos recolectores se convirtieron en choferes, mientras que uno de ellos ahora es supervisor. Vemos cómo crece cada familia con su vivienda", dijo a Cadena 3 Noelia Segovia, presidenta de la cooperativa.

A la recolección, se le suma la separación de residuos y una carpintería. En la asociación, con restos de telgopor, plástico y cartón fabrican bancos de plaza y útiles escolares, por ejemplo.

Además del medio ambiente, en Creando Conciencia hacen énfasis en la integración de personas que alguna vez estuvieron excluidas. También colaboran donando alimentos a cinco comedores escolares.

"Estos son los emprendimientos que nos gusta apoyar. Somos unas cooperativa genuina a donde se pueden generar muchos puestos de trabajo", agregó.