+++
author = ""
date = 2021-02-12T03:00:00Z
description = ""
image = "/images/mosquitos-1.jpg"
image_webp = "/images/mosquitos-1.jpg"
title = "Invasión de mosquitos en el AMBA: ¿A qué se debe?"

+++

**_Las lluvias y los vientos de los últimos días produjeron un incremento notable en la población de este insecto en la región metropolitana. Los especialistas aseguran que se irán cuando cambien las condiciones climáticas._**

La proliferación de mosquitos observados en los últimos días en el Área Metropolitana de Buenos Aires (AMBA) fue producto de las lluvias y los vientos, aunque ninguno de ellos es el Aedes Aegypti, transmisor de dengue, zika o fiebre chikungunya. "Dentro de unos días desaparecerán porque no tendrán donde dejar sus huevos" , afirmó el biólogo Nicolás Schweigmann.

El investigador del Conicet y profesor de la facultad de Ciencias Exactas y Naturales de la UBA dijo a Télam que “el registro de grandes crecimientos en la población del mosquito como el que apareció en el AMBA, el Aedes Albifasciatus es un fenómeno habitual de esta región vinculado a las condiciones meterológicas".

“La hembra del Aedes Albifasciatus pone huevos en el barro, en el pasto o en el borde de los charcos, y mientras no llueve sigue poniendo huevos en el mismo lugar; entonces cuando después de una sequía viene una lluvia todos esos huevos eclosionan juntos y provocan un súbito e importante aumento de la población de mosquitos”, indicó.

El biólogo apuntó que “el Aedes Albifasciatus es un mosquito de zonas rurales, es el más austral del planeta porque se lo registra incluso en la patagonia y no tiene problemas con el frío porque sus huevos resisten hasta diez grados bajo cero. Esta adaptado para picar animales silvestres por lo que cuando pica a las personas es doloroso ya que también es común que nos piquen varios a la vez”.

“Este mosquito no tiene como proliferar en las zonas urbanas porque el avance del asfalto y el concreto le quitó los charcos y el barro que necesita para depositar los huevos", dijo el especialista.

El especialista agregó que es probable que todos estos mosquitos que se vieron este jueves hayan sido arrastrados por el viento desde zonas periurbanas como los humedales de Ezeiza y por eso no tienen futuro en la ciudad, de acá a unos días vamos a ver como va a empezar a desaparecer esta población porque no tienen donde dejar sus huevos”,

Schweigman detalló que “la Argentina tiene más de 240 especies de mosquitos, en general como todo insecto son más activos en el verano,

“El Aedes Aegypti prefiere la sangre humana, es un mosquito diurno, no pica de noche, los que nos pican de noche son los de tipo Culex Pipiens que en general prefieren la sangre de las aves”, aclaró.

El investigador subrayó que “para cuidarnos deberíamos hacerlo durante todo el año con acciones diferenciales de prevención de acuerdo a cada época del año; y comentó que “los estudios señalan que año tras año el Aedes Aegypti va aumentando su presencia territorial en Buenos Aires porque las campañas de prevención no logran la efectividad necesaria”.