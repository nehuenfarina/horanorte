+++
author = ""
date = 2021-01-22T03:00:00Z
description = ""
image = "/images/regreso-a-clases-en-caba-20200820-1004031.jpg"
image_webp = "/images/regreso-a-clases-en-caba-20200820-1004031.jpg"
title = "Implementarán en febrero programas de recuperación y revinculación escolar"

+++

**_El ministro de Educación de la Nación, Nicolás Trotta, recordó que para recuperar el vínculo con la escuela buscará que "cada una de nuestras niñas y niños vuelvan y se queden en la escuela"._**

Las provincias implementarán a partir de febrero programas de recuperación de contenidos educativos y espacios para la revinculación de estudiantes que a raíz de la pandemia de coronavirus tuvieron trayectorias escolares discontinuas el año pasado y para ello pondrán en marcha unidades pedagógicas, actividades especiales y tutorías docentes, ante el inicio del ciclo lectivo 2021.

Varias de esas actividades se enmarcan en el Programa Acompañar, Puentes de Igualdad implementado por el Ministerio de Educación de la Nación, cuyo titular, Nicolás Trotta, recordó este miércoles que para recuperar el vínculo con la escuela buscará que "cada una de nuestras niñas y niños vuelvan y se queden en la escuela" con un plan de "codiseño federal, que está dirigido a complementar iniciativas existentes y generar solidaridades territoriales atendiendo a las necesidades de cada región".

En ese sentido, la ministra de Educación de Neuquén, Cristina Storioni, resaltó como un desafío "proponer una agenda desde ahora, durante la post-pandemia inmediata y a mediano plazo" con "políticas de solidaridad, reconocimiento, participación y diálogo".

Al describir el objetivo de los programas que arrancan en febrero, la funcionaria explicó que se apela a "una solidaridad que tiene su trama en lo que no es escolar pero que se pone al servicio de lo escolar, para propiciar unos modos hacia el 'adentro' de la escuela que aminoren frustraciones y decepciones".

En la provincia de Buenos Aires se reanudarán en febrero los procesos de intensificación de la enseñanza, con el objetivo de acompañar a los estudiantes que durante 2020 tuvieron trayectorias educativas discontinuas, dado que según una resolución del área educativa se contempla como un mismo ciclo a la unidad pedagógica 2020-2021 a raíz de la pandemia.

Durante enero, la provincia lleva adelante el ciclo "Verano ATR" (Ante Todo Responsables), destinado a más de 300 mil estudiantes que realizarán actividades pedagógicas, lúdicas y recreativas para abordar los contenidos curriculares priorizados en cada año y nivel de enseñanza.

En Córdoba, 232 escuelas de la provincia van a disponer de docentes tutores para empezar un trabajo de apoyo para quienes por diversas circunstancias discontinuaron la presentación de los trabajos escolares, y de este modo reforzar su situación educativa.

En Santa Fe, donde las clases comenzarán el 15 de marzo, los docentes regresarán a las escuelas el 17 de febrero para completar con los alumnos de 7mo grado y 5to y 6to año de la secundaria el "período de intensificación pedagógica" que culminará el 10 de marzo.

La ministra de Educación, Adriana Cantero, expresó que "a partir de ese momento vamos a trabajar con los grupos prioritarios hasta el 10 de marzo, complementando y asegurando las competencias básicas del nivel".

En Mendoza, el gobernadro Rodolfo Suarez ratificó que "el 10 de febrero comienzan los chicos de trayectorias débiles que no han podido acceder durante el 2020 a las clases por la falta de acceso tecnológico".

El director general de escuelas, José Thomas, explicó que "el plan que se maneja en el gobierno de la provincia es que haya asistencia con la mayor intensidad posible para los primeros meses del año", ya que fueron casi 19.000 los estudiantes con inconvenientes.

En Santiago del Estero, el 17 de febrero volverán a clases los alumnos de los años superiores del Nivel Primario y Secundario que no pudieron alcanzar los objetivos en diciembre quienes, según la ministra de Educación, Mariela Nassif, van a trabajar "en unidades pedagógicas para que los contenidos no abarcados se puedan unir con los de este año".

Nassif señaló que "es esencial que los chicos puedan estar incluidos en el sistema educativo" y sostuvo que "están puestos en marcha todos los mecanismos para que nuestros alumnos avancen y recuperen los contenidos".

"Es un trabajo artesanal, para que la escuela sea el mejor lugar donde los chicos puedan estar", añadió y recordó que el 8 de febrero los equipos de gestión, administrativos y de maestranza retomarán el acondicionamiento y la organización de las instituciones educativas.

Además, detalló que están "incluidos en el programa Acompañar: Puentes de Igualdad, que significa que muchas instituciones gubernamentales y no gubernamentales nos van a acompañar en este proceso de revincular a los alumnos que quizás no pudieron cumplir con los objetivos del 2020 o que han estado desvinculado del ámbito educativo".

En Río Negro, donde las clases comienzan el 3 de marzo, "los docentes podrán comenzar con tareas de vinculación desde el 8 de febrero y los cursos no podrán superar una capacidad máxima de 15 estudiantes", señalaron fuentes de la cartera educativa provincial.

La ministra de Educación de Tierra del Fuego, Analía Cubino, detalló que para reactivar el dictado de clases "lo primero será concluir las actividades de los estudiantes que están terminando el nivel secundario, y que corresponden al ciclo 2020".

"Estos alumnos volverán a tener contacto con sus docentes desde el 17 de febrero y hasta el 30 de marzo, de manera virtual y con la realización de un trabajo final integrador", acotó.

Jujuy, por su parte, desarrolló en noviembre y diciembre un programa destinado a la revinculación social escolar, de carácter optativo, del cual participaron los alumnos de primero a sexto grado de la primaria y de primero a cuarto de secundaria.

Las escuelas de educación técnica y agrotécnica realizaron prácticas en talleres, laboratorios y espacios productivos, y cada tres horas un intervalo de quince minutos para la higienización.

En esa provincia, las clases comenzarán el 17 de febrero de forma presencial, día hábil de por medio, en los horarios habituales para diferentes turnos. Los días en que no asistan deberán cumplir con tareas que los docentes les asignen.

En Misiones se instalarán Centros de Apoyo Pedagógicos y Tecnológicos en cada municipio para acompañar a los estudiantes de sexto grado hasta el nivel superior, a alcanzar los contenidos del ciclo lectivo 2020-21, informó hoy el Ministerio de Educación, Ciencia y Tecnología local.

Los Centros de Apoyo comenzarán a funcionar en febrero, serán gratuitos y estarán equipados con computadoras, wifi, entornos virtuales formativos y serán espacios no escolares que buscarán garantizar y democratizar el acceso a la educación.

En Neuquén, el Ministerio de Educación implementará el programa "Puentes de Verano", para generar estrategias de regreso y revinculación escolar a trayectorias educativas interrumpidas total o parcialmente durante el 2020.

La ministra de Educación y presidenta del Consejo Provincial de Educación (CPE), Cristina Storioni explicó que "se proyecta acompañar a más de 2.200 trayectorias de estudiantes de secundaria y tercer ciclo de primaria, para lo que se dispondrán 35 nodos que desplegarán diferentes actividades".

"Acompañar implica asumir políticas de solidaridad, reconocimiento, participación", añadió y destalló que una de las acciones del Puentes de Verano consiste en la implementación de Centros Educativos Comunitarios en los que se desarrollarán espacios artísticos, a partir de talleres de: música, medios audiovisuales, literarios, artes visuales, oralidad, lúdico y organización escolar.

Asimismo, los programas Puentes de Verano que se vinculan con el nacional Acompañar se implementan en Chaco, Salta, Catamarca y otras provincias.