+++
author = ""
date = ""
description = ""
image = "/images/africa.jpg"
image_webp = "/images/africa.jpg"
title = "El PJ bonaerense repudió al periodista de La Nación que habló del \"africanizado conurbano\""

+++

**_El PJ provincial salió así al cruce del artículo titulado "La madre de todas las batallas", donde Sirvén califica a la provincia de Buenos Aires como "ese territorio inviable en cuyo africanizado conurbano se deciden electoralmente los destinos de la Patria"._**

El Partido Justicialista bonaerense repudió al periodista Pablo Sirvén, quien en una columna publicada en el diario La Nación mencionó al "africanizado conurbano", y condenó "la intencionalidad insultante, discriminadora y excluyente de un lenguaje agresivo que su emisor esgrime con fines eminentemente políticos".

El PJ provincial salió así al cruce del artículo titulado "La madre de todas las batallas", donde Sirvén califica a la provincia de Buenos Aires como "ese territorio inviable en cuyo africanizado conurbano se deciden electoralmente los destinos de la Patria".

"El discurso divisionista y la violencia verbal de los cultores del odio, de nada sirven", sostuvo.

"La cultura del descarte encuentra un factor vigorizante en el lenguaje de la discriminación. Las nuevas narrativas del odio hunden sus raíces en aquellas parcelas psicológicas donde el terreno es fértil para justificar los males del país", dijo el PJ en un comunicado.

Señaló además que "con el advenimiento del Peronismo las minorías oligárquicas derrotadas diseñaron una pedagogía excluyente que aún perdura en algunas cavernas mediáticas, académicas, artísticas, empresariales y partidarias".

"En los años electorales el arsenal semántico descalificador suele manifestarse de manera brutal, porque nada es más brutal que el insulto a la dignidad humana. Y en el fondo se libra la lucha por la dignidad humana, por el reconocimiento de derechos y la reivindicación de identidades agredidas", añadió.

"Sirvén afirma que en el 'africanizado conurbano se deciden electoralmente los destinos de la Patria'. Si a la hipérbole le quitamos su hojarasca, queda al desnudo la intencionalidad insultante, discriminadora y excluyente de un lenguaje agresivo que su emisor esgrime con fines eminentemente políticos para consumo de un público agrietado que promueve la cultura del descarte del otro, del distinto, del mayoritario", aseveró

"Considerar al conurbano bonaerense como un espejo de África engloba una mirada falsa. África, cuna de Nelson Mandela, tiene múltiples idiosincrasias que hacen de este vasto continente una realidad plural en la que se combinan valores humanistas innegables con modelos de vida diferentes y hasta superiores a los proclamados por 'occidente'", añadió el PJ provincial.

Y sostuvo que "a pesar de la lógica difamatoria de Sirvén, la diversidad también es la cualidad sobresaliente del conurbano que se manifiesta en un mosaico de pueblos unidos por su decidida lucha a favor de la dignidad de cada uno y del conjunto".

"Nuestra esperanza está depositada en los millones de hermanos y hermanas que han decidido construir un proyecto de vida en nuestra provincia", aseveró el partido gobernante, y aseguró que "lejos de pensar la realidad en función electoralista, preferimos gestionar los numerosos desafíos que se nos presentan sin promover el descarte ni la discriminación de nadie".