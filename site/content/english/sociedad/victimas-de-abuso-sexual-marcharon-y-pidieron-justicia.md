+++
author = ""
date = 2021-01-29T03:00:00Z
description = ""
image = "/images/victimas.jpg"
image_webp = "/images/victimas.jpg"
title = "Víctimas de abuso sexual marcharon y pidieron justicia"

+++

**_Se trata de familiares y amigos de varias jóvenes de entre 17 y 23 años que denunciaron haber sido abusadas sexualmente por un hombre de 27 años, hijo de un violador serial ya condenado en 2009 a 40 años de cárcel por nueve violaciones._**

Los padres, familiares, amigos y vecinos de las jóvenes que denunciaron haber sido abusadas sexualmente por el hijo de un violador serial en la localidad de Marcos Paz, realizaron una marcha en reclamo de justicia y exigieron la detención del sospechoso.

El encuentro se llevó a cabo en la tarde del jueves en la plaza San Martín de la mencionada localidad del oeste de la Provincia de Buenos Aires, bajo las consignas “no es no” y “hay un violador serial suelto”.

Se trata de familiares y amigos de varias jóvenes de entre 17 y 23 años que denunciaron haber sido abusadas sexualmente por un hombre de 27 años, hijo de un violador serial ya condenado en 2009 a 40 años de cárcel por nueve violaciones cometidas durante 2005 en la zona oeste del conurbano.

Según las fuentes de la investigación, el sospechoso continúa en libertad porque todavía no fue imputado formalmente por la fiscalía que interviene en el expediente.

Valeria Carreras, abogada de una de las víctimas, indicó a Télam que el martes se presentó a declarar un testigo clave que contó lo que sucedió la mañana del 25 de diciembre pasado, cuando el sospechoso interceptó a una joven que volvía a su domicilio en horas de la madrugada, la engañó para subir al vehículo en el cual se trasladaba y la llevó a un descampado donde la violó.

"Esto es fundamental, porque confirma lo que dijo la víctima", dijo a Télam la letrada.

Para la abogada, existen pruebas suficientes para pedir la imputación formal del sospechoso por "abuso sexual con acceso carnal", sin embargo aún restan los resultados de laboratorio de las muestras de sangre colectada para el cotejo de ADN con los elementos de prueba.

Fuentes judiciales informaron a Télam que el mismo sospechoso está denunciado en tres causas penales que sustancia personal de la Unidad Funcional de Instrucción (UFI) 2 de Mercedes, aunque en ninguna está formalmente imputado ya que resta producir más prueba.