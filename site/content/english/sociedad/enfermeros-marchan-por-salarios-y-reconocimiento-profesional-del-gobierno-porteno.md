+++
author = ""
date = 2021-02-02T03:00:00Z
description = ""
image = "/images/enfermeros.jpg"
image_webp = "/images/enfermeros.jpg"
title = "Enfermeros marchan por salarios y reconocimiento profesional del Gobierno porteño"

+++

**_La delegación que irá a la reunión estará integrada por representantes de la comisión directiva de ALE y de hospitales "autoconvocados y movilizados" como Gutiérrez, Santojanni, Fernández y Sardá, entre otros._**

La Asociación de Licenciados en Enfermería (ALE) marchaba para acompañar una nueva audiencia con funcionarios del Gobierno porteño, ante quienes vienen reclamado por salarios y el reconocimiento profesional de los trabajadores del sector.

Los enfermeros y las enfermeras se concentraron a las 11 frente a la sede del Ministerio de Salud porteño, Monasterio 480, informaron a Télam voceros gremiales.

La delegación que tenía previsto ir a la reunión está integrada por representantes de la comisión directiva de ALE y de hospitales "autoconvocados y movilizados" como Gutiérrez, Santojanni, Fernández y Sardá, entre otros.

Andrea Ramírez, enfermera del Hospital Ramos Mejía y referente de la ALE, sostuvo que "el 2021, lejos de la 'pospandemia', arrancó con rebrote del virus y la presión de una campaña de vacunación que recae fuertemente sobre nuestro sector".

"Hasta ahora la respuesta del Gobierno de CABA y muchas gestiones de hospitales, con aval de la conducción de SUTECBA (gremio de Municipales), viene siendo más contratos precarios, ni una palabra de aumento salarial y postergación del reconocimiento profesional con la inclusión en la Ley N°6035 para que, como enfermería, tengamos los mismos derechos que todos los profesionales de la salud", dijo.

Carolina Cáceres, del hospital Tornú, afirmó que "ante una realidad económica que se nos hace insoportable nos movilizamos exigiendo urgente recomposición salarial, reconocimiento profesional, capacitación en servicio, pase a planta de todos los contratados bajo condiciones de Covid y cese de persecución laboral en hospitales de la Ciudad".