+++
author = ""
date = 2021-02-12T03:00:00Z
description = ""
image = "/images/6026ab828f4f0_1004x565.jpg"
image_webp = "/images/6026ab828f4f0_1004x565.jpg"
title = "Hallan una nueva especie de rincosaurio de 230 millones de años de antigüedad"

+++

**_Paleontólogos argentinos encontraron en el Parque Nacional Talampaya fragmentos del cráneo, parte del sacro, las primeras vértebras de la cola y parte de la cadera de la especie "Elorhynchus carrolli"._**

Una nueva especie de rincosaurio, **un reptil cuadrúpedo, de unos tres metros de longitud, herbívoro, que vivió hace 230 millones de años y compartió su hábitat con los primeros dinosaurios**, fue hallada en el Parque Nacional Talampaya, al centro-oeste de la provincia de La Rioja, por paleontólogos argentinos.  
  
El equipo de investigadores bautizó esta nueva especie con el nombre de Elorhynchus carrolli, en homenaje de la bióloga Eloisa Argarañaz, quien participó de las campañas en las que se produjo el hallazgo, en la Formación Chañares de esa provincia.  
  
Los investigadores hallaron “fragmentos del cráneo de esta nueva especie, incluyendo premaxilar, maxilar y dentario, parte del sacro, las primeras vértebras de la cola y parte de la cadera”, informó **Martín Ezcurra**, uno de los jefes de la Sección Paleontología de Vertebrados del Museo Argentino de Ciencias Naturales (Macn) e investigador del Conicet.  
  
"El cráneo tiene una dentición muy especializada compuesta por numerosas hileras de dientes y un pico óseo en el hocico que le podría haber servido para el procesamiento del alimento previamente a ser tragado, a diferencia de lo que sucede en la gran mayoría de los reptiles”, especificó Ezcurra, autor principal del estudio, publicado recientemente en la revista científica **Journal of Systematic Palaentology**.  
  
El investigador explicó que “cuando aparecen las primeras faunas de dinosaurios, los rincosaurios eran formas muy abundantes, de hecho, en el Valle de la Luna, en San Juan, donde se registraron algunos de los dinosaurios más antiguos que se conocen, los rincosaurios son los más abundantes en ese momento”.

Todos los rincosaurios aparecidos en Argentina "pertenecían a la especie llamada Hyperodapedon ischigualastensis, hasta ahora que dieron a conocer a esta nueva especie, la Elorhynchus carrolli, que es "más antigua y la podemos distinguir por ciertas características en su cadera y en las primeras vértebras de la cola”, destacó Ezcurra.

Los rincosaurios tenían sus patas hacia los costados de su cuerpo y la panza cercana al piso, de forma semejante a los lagartos overos que vemos en la actualidad, y tienen un lazo de parentesco lejano con los cocodrilos y dinosaurios, consignó la Agencia CTyS-UNLaM.

“Los restos hallados pertenecen a varios individuos, que fueron recolectados en diversas campañas realizadas en Talampaya durante la última década y a partir de los cuales pudimos reconocer que se trataba de una nueva especie”, precisó el paleontólogo Lucas Fiorelli del Centro Regional de Investigaciones Científicas y Transferencia Tecnológica de La Rioja (Crilar-Conicet).

El equipo de investigadores que dio con estos animales se especializa desde hace diez años “en el estudio de la evolución de los arcosauriformes, tanto en lo que es su anatomía, sus relaciones de parentesco, en sus patrones evolutivos y en todo lo que es paleobiología, es decir, en cómo todos estos animales cumplían roles en las comunidades continentales triásicas de América del Sur y del mundo”, explicó Julia B. Desojo, investigadora del Conicet en la División Paleontología de Vertebrados del Museo de La Plata (MLP-UNLP).

Este hallazgo permite conocer mejor la temprana evolución de este grupo de animales en el noroeste argentino, “en particular, permite observar cómo se produjo la transición de los rincosaurios que vivieron antes del surgimiento de los primeros dinosaurios hasta los rincosaurios que llegaron a convivir con estos reptiles desde los 233 millones de años hasta que se extinguieron hace unos 227 millones de años”, explicó Ezcurra.

“A su vez, esta especie nos ayuda a establecer mejor las correlaciones temporales entre la Formación Chañares y otras formaciones del mundo, principalmente del hemisferio sur, para el Triásico, porque esta especie está muy emparentada con especies del sur de Brasil, de Tanzania y de la India, los cual indica que estas formaciones tenían edades aproximadamente similares”, agregó.