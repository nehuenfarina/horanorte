+++
author = ""
date = ""
description = "De norte a sur, centros de atención telefónica brindan ayuda para estar informados sobre la pandemia del coronavirus en la Argentina."
image = "/images/5f5fdd923b627_1004x565-1.jpg"
image_webp = "/images/5f5fdd923b627_1004x565.jpg"
title = "Los 0800 Covid implementados por 12 provincias reciben de 200 a 3.500 llamadas por día"

+++
###### _Doce provincias implementaron sistemas de consulta telefónica sobre coronavirus que reciben entre 200 a 3500 llamados diarios._ 

**Buenos Aires**

En esta provincia, unos **160 médicos, estudiantes avanzados de la carrera y voluntarios** que integran el Centro de Telemedicina de Covid-19 (CeTec-19) del Ministerio de Salud bonaerense tienen a cargo la búsqueda de casos sospechosos de coronavirus, y desde su apertura, a fines de abril, **ya realizaron 30.154 llamadas** que derivaron en la internación o aislamiento de los positivos.

Además, unos 100 profesionales, estudiantes de medicina y enfermería de la **Universidad Nacional del Sur, en Bahía Blanca**, junto con expertos en informática se desempeñan en **el servicio telefónico 147** donde se realiza seguimiento de pacientes con Covid y contactos estrechos.  
"Tenemos alrededor de 70 llamados diarios y funciona de lunes a viernes de 9 a 15", dijo Horacio Romano, coordinador del triage telefónico.

**En el norte del país**

La municipalidad de la ciuda**Formosa** creó el programa **"Estamos con vos, te escucho"**, en el que de 10 a 22 y todos los días, psicólogos, acompañantes terapéuticos y trabajadoras sociales asisten a vecinos "para la contención y acompañamiento permanente en los términos de una salud comunitaria integral", informaron este lunes a Télam fuentes comunales.

También **Jujuy** implementó dos sistemas de asistencia: consultas telefónicas que son recibidas por "65 operadores, 17 médicos y 10 coordinadores para brindar asistencia, información o realizar derivaciones", según el informe del Ministerio de Salud local, que agregó que ya se hicieron **"16.224 atenciones".**  
Y una segunda opción vía página web de la cartera sanitaria, donde se puede solicitar la asistencia de un profesional por Zoom, en un sistema que funciona de 8 a 20 y realiza alrededor de 150 y 200 atenciones diariasDesde **La Rioja**, el médico Gonzalo Calvo, integrante del COE local, explicó que **la provincia tiene tres áreas de trabajo**: primero, se evacúan las dudas de la gente con respecto a si tienen o no coronavirus; luego se hace el seguimiento telefónico y, por último, otra línea donde voluntarios y estudiantes de los últimos años de medicina de la Universidad Nacional de La Rioja realizan la investigación de los nexos para evaluar los contactos laborales o cercanos que pudo tener el paciente.  
El COE asiste y controla en estos momentos más de 1200 domicilios, **tiene alrededor de 500 llamados diarios** a la línea telefónica 107, y entre 130 y 250 al área asistencial del Gobierno 911.

En **Santiago del Estero**, la línea del Servicio de Emergencia y Accidentologia de Santiago del Estero 107 (Sease) es la red que recibe las consultas por coronavirus y emergencias: "Tenemos 15 líneas rotativas que se sumaron, antes teníamos solo cuatro", según detalló a Télam el director de este organismo, Ariel Trejo,  
**"Cerramos el mes de agosto con 40 mil llamadas**, un promedio de 1.333 por día, de las cuales el 80% son consultas por Covid-19 y el 20% restante es por trauma", explicó.  
Por otro lado, existe una línea telefónica de una red de profesionales que acompañan a las personas con Covid-19 positivo y a sus familias durante los 14 días de aislamiento: "Son 36 médicos, 12 psicólogos, 10 trabajadores sociales, 10 educadores sanitarios y un grupo de profesionales para embarazadas", explicó una de las coordinadoras, Adriana Cortese, referente del Área de Salud Mental.

**Provincias del centro**

**Córdoba** también cuenta con dos líneas telefónicas para llamar y hacer consultas sobre síntomas sospechosos de coronavirus: una que depende del Ministerio de Salud y otra que corresponde a la Administración Provincial de Seguro de Salud (Apross), la obra social de la provincia, que recibe **un promedio diario de 400 consultas.**

En **Entre Ríos**, al enviar un mensaje de Whatsapp, "ER Bot" responde con un menú que responde consultas de forma gratuita y las 24 horas, al tiempo que se dispuso de una línea oficial del Ministerio de Salud donde **se reciben cerca de 1.000 llamados por semana**, el 35% de ellos de personas de contacto estrecho con positivos de Covid-19, según confirmó el Gobierno provincial.

En el caso de **Santa Fe**, se reciben un promedio de 700 a 800 llamadas por día, aunque la línea Covid atendió 50.355 consultas solo en los primeros diez días de septiembre, cifra que contrasta con **los 28.405 que atendió durante todo el mes de julio**informó el Ministerio de Salud provincial.  
La directora provincial de Epidemiología, Carolina Cudós, explicó hoy a Télam que a partir de agosto las autoridades "duplicaron la cantidad de médicos atendiendo, con lo cual se descomprimió" el sistema.

También la provincia de **Mendoza** cuenta con la línea 0800 COVID para responder consultas generales acerca del coronavirus: "Tenemos **aproximadamente 3.500 llamadas diarias. La mayoría por la mañana**, lo que genera demoras", explicó Karina Rosas, coordinadora del Centro de Atención Ciudadana.

**En el sur**

El **Plan Río Negro +60** se creó con **una línea gratuita de acompañamiento a personas mayores**, programas de radio y televisión, contenidos digitales, contacto con Centros de Adultos Mayores, ayuda voluntaria y atención psicológica.  
"Las personas mayores no quieren hablar de la pandemia", precisó el subsecretario de Participación Social y Articulación Solidaria, Rafael Taborda, al tiempo que a mediados de abril se recibieron más de 1000 llamadas.

Se presentó Río Negro +60, un plan destinado a acompañar y ayudar a los adultos mayores de 60 años, durante el aislamiento obligatorio. Por eso, se habilitó un 0800 gratuito y exclusivo para que puedan hacer consultas, recibir asistencia psicológica y realizar trámites. MiráEl Ministerio de Salud del **Chubut** informó que en las últimas semanas se triplicaron las llamadas al dispositivo telefónico provincial 0800 CORO, que la cartera sanitaria puso a disposición desde que comenzó a regir el ASPO.  
"Recibimos en promedio **unas 350 llamadas por día de adultos jóvenes**, en general, que presentan mucha angustia y la mayoría muestra arrepentimiento por haber transgredido las recomendaciones que se brindan desde Salud", indicó a Télam el coordinador del dispositivo, Marcelo Ballari.

En **Neuquén**, el 0800 COVID durante la última semana recibió 1.600 llamadas, a un promedio de poco más de 200 por día, según información del Ministerio de Salud.